﻿using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using WinSCP;

namespace TopasIntegration
{
    class Program
    {
        static void Main(string[] args)
        {
            var Mode = ConfigurationManager.AppSettings["Mode"].ToString();
            var ProcessingMode = ConfigurationManager.AppSettings["ProcessingMode"].ToString();
            var TopasSchema = ConfigurationManager.AppSettings["TopasSchema"].ToString();
            var GudangSysSchema = ConfigurationManager.AppSettings["GudangSysSchema"].ToString();
            var ClientAccount = ConfigurationManager.AppSettings["ClientAccount"].ToString();
            var WarehouseCode = ConfigurationManager.AppSettings["WarehouseCode"].ToString();
            string TopasDateString = DateTime.Now.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            string TopasTimeString = DateTime.Now.ToString("HHmmss", System.Globalization.CultureInfo.InvariantCulture);
            string TopasSQL = "";
            string GSysSQL = "";
            DateTime datestamp = DateTime.Now;
            try
            {
                Console.ForegroundColor = ConsoleColor.White;
                if (Mode == "StockSync")
                {
                    iDB2Command sqlCmd = new iDB2Command();
                    Console.WriteLine("Cleaning 8888888 locations");
                    //sqlCmd.CommandText = "update " + GudangSysSchema + ".stocklist set avqty=0, s1qty=0, s2qty=0 where stockitemid in " +
                    //    "(select b.stockid  from " + TopasSchema  + ".p1pmstp a,  " + GudangSysSchema + ".stockmaster b " +
                    //"where a.pmstok + a.pmforc = 0 and a.pmbrcd = '149' and b.locationcode = '88888888' "+
                    //"and a.pmpno = b.itemcode and a.pmfran = b.lotnumber and b.avqty > 0)";
                    //Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                    Console.WriteLine("Cleaning -AvQty locations");
                    sqlCmd = new iDB2Command();
                    sqlCmd.CommandText = "update " + GudangSysSchema + ".stocklist set avqty=0 where avqty<0";
                    Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                    Console.WriteLine("Cleaning -OrQty locations");
                    sqlCmd = new iDB2Command();
                    sqlCmd.CommandText = "update " + GudangSysSchema + ".stocklist set orqty=0 where orqty<0";
                    Db2GetDataTable(sqlCmd, Db2GudangSysConnection());


                    Console.WriteLine("Resync -S1Qty locations");
                    sqlCmd = new iDB2Command();
                    sqlCmd.CommandText = "update " + GudangSysSchema + ".stocklist set s1qty=avqty +orqty where  s1qty<>avqty +orqty";
                    Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                    Console.WriteLine("Updating Receiving StatusCode");
                    sqlCmd = new iDB2Command();
                    sqlCmd.CommandText = "update  " + GudangSysSchema + ".rcvorderdetails set statuscode ='C' where ordergoqty = binokqty +binleqty +checkleqty and statuscode <> 'C'";
                    Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                }

                if (Mode == "Receive")
                {
                    if (ProcessingMode.IndexOf("MasterData") != -1 || ProcessingMode.ToString() == "")
                    {
                        //Update Quality Flag

                        Console.WriteLine(DateTime.Now.ToString() + " : Processing Quality Flag...");
                        iDB2Command updateCmd = new iDB2Command();
                        updateCmd.CommandText = "UPDATE " + GudangSysSchema + ".ItemMaster set QualityFlag=0";
                        Db2GetDataTable(updateCmd, Db2GudangSysConnection());
                        updateCmd = new iDB2Command();
                        updateCmd.CommandText = "UPDATE " + GudangSysSchema + ".ItemMaster A  SET A.QualityFlag =(SELECT 1 FROM " + TopasSchema + ".P1QUALP B  WHERE a.Itemcode = b .PQPNO and PQSTAT='A'  )";
                        Db2GetDataTable(updateCmd, Db2GudangSysConnection());
                        Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        //Export Buyer Code Information File - P1BUYCP

                        //Client Account
                        Console.WriteLine(DateTime.Now.ToString() + " : Processing Client Account...");
                        TopasSQL = "select * from " + TopasSchema + ".P1GIM0P";
                        GSysSQL = "select * from " + GudangSysSchema + ".TenantMaster";
                        iP1GIM0P(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                        Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        //Client Account

                        //Supplier Master 
                        Console.WriteLine(DateTime.Now.ToString() + " : Processing Supplier Master...");
                        iP1CSUPP(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                        Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        //Supplier Master

                        //Denial Reason
                        Console.WriteLine(DateTime.Now.ToString() + " : Processing Denial Reason...");
                        TopasSQL = "select * from " + TopasSchema + ".REFERP where REFCAT = '834' ";
                        GSysSQL = "select * from " + GudangSysSchema + ".DenialReason";
                        iREFERP(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                        Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        //Denial Reason


                        //Customer Master
                        Console.WriteLine(DateTime.Now.ToString() + " : Processing Customer Master...");
                        TopasSQL = "select * from " + TopasSchema + ".P1CUSTP";
                        GSysSQL = "select * from " + GudangSysSchema + ".CustomerMaster";
                        iP1CUSTP(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                        Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        //Customer Master

                        //Location Master Bin Type -P2ELOCP
                        Console.WriteLine(DateTime.Now.ToString() + " : Processing Location Master...");
                        TopasSQL = "select * from " + TopasSchema + ".P2ELOCP";
                        GSysSQL = "select * from " + GudangSysSchema + ".WHBinTypeRef";
                        iP2ELOCP(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                        Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        //Location Master Bin Type -P2ELOCP

                        //Item Master Dimension - P1PARTP
                        Console.WriteLine(DateTime.Now.ToString() + " : Processing Item Master...");
                        TopasSQL = "select * from " + TopasSchema + ".P1PARTP";
                        GSysSQL = "select * from " + GudangSysSchema + ".ItemMaster";
                        iP1PARTP(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                        Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        //Item Master Dimension - P1PARTP
                    }
                    if (ProcessingMode.IndexOf("Receiving") != -1 || ProcessingMode.ToString() == "")
                    {

                        // Receiving
                        Console.WriteLine(DateTime.Now.ToString() + " : Processing ASN...");
                        TopasSQL = "select * from " + TopasSchema + ".P1BCBHP  WHERE BHFLAG = ''";
                        GSysSQL = "select * from " + GudangSysSchema + ".RcvOrderHeaders WHERE StatusCode = 'N' ";
                        iP1BCBHP(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                        Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        //Receiving
                    }

                    if (ProcessingMode.IndexOf("Shipping") != -1 || ProcessingMode.ToString() == "")
                    {
                        //Execute Topas Movement
                        //ExecuteCommand(ConfigurationManager.AppSettings["Movement_Command"].ToString());
                        //ExecuteCommand(ConfigurationManager.AppSettings["Block_Command"].ToString());
                        //ExecuteCommand(ConfigurationManager.AppSettings["Unblock_Command"].ToString());
                        iDB2Command sqlCmd = new iDB2Command();
                        //sqlCmd.CommandText = "update " + TopasSchema +".P1BLKIP set BKSTAT='X', BKUPDT='" + TopasDateString+ "', BKUPTM='" + TopasTimeString + "' where BKSTAT='';";
                        //DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());


                        string DomesticIntegration = "1";
                        string ExportsIntegration = "1";
                        string EMIntegration = "1";

                         sqlCmd = new iDB2Command();
                        sqlCmd.CommandText = "SELECT Description,VALUE FROM " + GudangSysSchema + ".Parameters  WHERE Description = 'DomesticIntegration'";
                        DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                        List<Parameter> Parameter = new List<Parameter>();
                        Parameter = ConvertDataTable<Parameter>(gudangsysdata);
                        foreach (var ll in Parameter)
                        {
                            DomesticIntegration = ll.VALUE.Trim();
                        }

                        sqlCmd = new iDB2Command();
                        sqlCmd.CommandText = "SELECT Description,VALUE FROM " + GudangSysSchema + ".Parameters  WHERE Description = 'ExportsIntegration'";
                        gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                        Parameter = new List<Parameter>();
                        Parameter = ConvertDataTable<Parameter>(gudangsysdata);
                        foreach (var ll in Parameter)
                        {
                            ExportsIntegration = ll.VALUE.Trim();
                        }

                        sqlCmd = new iDB2Command();
                        sqlCmd.CommandText = "SELECT Description,VALUE FROM " + GudangSysSchema + ".Parameters  WHERE Description = 'EMIntegration'";
                        gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                        Parameter = new List<Parameter>();
                        Parameter = ConvertDataTable<Parameter>(gudangsysdata);
                        foreach (var ll in Parameter)
                        {
                            EMIntegration = ll.VALUE.Trim();
                        }


                        //Exports
                        if (ExportsIntegration == "1")
                        {
                            Console.WriteLine(DateTime.Now.ToString() + " : Processing Exports...");
                            TopasSQL = "select * from " + TopasSchema + ".P1BCEXHP  WHERE BHFLAG = '' ORDER BY BHBHNO";
                            GSysSQL = "select * from " + GudangSysSchema + ".SalesOrderHeaders WHERE StatusCode = 'N' and OrderTypeCode like 'EX_%' ";
                            iP1BCEXHP(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                            Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        }
                        //Exports

                        //Domestic
                        if (DomesticIntegration == "1")
                        {
                            Console.WriteLine(DateTime.Now.ToString() + " : Processing Domestic/EM...");
                            TopasSQL = "select * from " + TopasSchema + ".P1BCPHP  WHERE BHFLAG = '' AND BHMODE IN ('B','R')";
                            GSysSQL = "select * from " + GudangSysSchema + ".SalesOrderHeaders WHERE StatusCode = 'N' ";
                            iP1BCPHP(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                        }
                        //EM
                        if (EMIntegration == "1")
                        {
                            TopasSQL = "select * from " + TopasSchema + ".P1BCPHP  WHERE BHFLAG = '' AND BHMODE IN ('E') and BHROUT <> 'RTI'";
                            GSysSQL = "select * from " + GudangSysSchema + ".SalesOrderHeaders WHERE StatusCode = 'N' ";
                            iP1BCPHP(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                            Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        }

                        TopasSQL = "select * from " + TopasSchema + ".P1BCPHP  WHERE BHFLAG = '' AND BHMODE IN ('E') and BHROUT = 'RTI'";
                        GSysSQL = "select * from " + GudangSysSchema + ".SalesOrderHeaders WHERE StatusCode = 'N' ";
                        iP1BCPHP(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                        Console.WriteLine(DateTime.Now.ToString() + " -RTI EM Completed");
                    }

                    if (ProcessingMode.IndexOf("Stock") != -1 || ProcessingMode.ToString() == "")
                    {
                        //Topas Inventory P1STOC1P
                        Console.WriteLine(DateTime.Now.ToString() + " : Processing Topas Inventory...");
                        TopasSQL = "select * from " + TopasSchema + ".P1STOC1P";
                        GSysSQL = "select * from " + GudangSysSchema + ".ERPDailyInventory_Snapshot";
                        iP1STOC1P(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                        Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        //Topas Inventory 
                    }
                }

                if (Mode == "Send")
                {
                    if (ProcessingMode.IndexOf("Receiving") != -1 || ProcessingMode.ToString() == "")
                    {

                        // Receiving
                        Console.WriteLine(DateTime.Now.ToString() + " : Sending ASN...");
                        GSysSQL = "select * from " + GudangSysSchema + ".RcvOrderHeaders WHERE StatusCode = 'C' ";
                        iP1BP2PHP(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                        Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        //Receiving

                        try
                        {
                            ExecuteCommand(ConfigurationManager.AppSettings["BC_Command"].ToString());
                        }
                        catch (Exception e)
                        {
                        }

                        iDB2Command sqlCmd = new iDB2Command();
                        sqlCmd.CommandText = "SELECT * from " + TopasSchema + ".P1BP2PHP where PHSTAT = 'E' OR PHSTAT = 'T' OR PHSTAT = 'X'";
                        DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                        List<P1BP2PHP> P1BP2PHP = new List<P1BP2PHP>();
                        P1BP2PHP = ConvertDataTable<P1BP2PHP>(gudangsysdata);
                        foreach (var ll in P1BP2PHP)
                        {
                            if (ll.PHSTAT.Trim() == "E")
                            {
                                iDB2Command updateCmd = new iDB2Command();
                                updateCmd.CommandText = "UPDATE " + GudangSysSchema + ".RcvOrderDetails A " +
                                        "SET A.CustomField2= (SELECT b.P2FPNT FROM " + TopasSchema + ".P1BP2P2P B  " +
                                        "WHERE trim(a.Key1) = trim(b.P2BHNO) AND trim(a.Key2) = trim(b.P2BITM)  and trim(b.P2FPNT) <>''  ),  A.CustomField3='Error' " +
                                        "WHERE trim(A.Remark) = @JobId";

                                updateCmd.Parameters.AddWithValue("@JobId".ToUpper(), ll.PHJBID.ToUpper().Trim());
                                Db2GetDataTable(updateCmd, Db2GudangSysConnection());
                            }

                            if (ll.PHSTAT.Trim() == "T")
                            {
                                iDB2Command updateCmd = new iDB2Command();
                                updateCmd.CommandText = "UPDATE " + GudangSysSchema + ".RcvOrderDetails A " +
                                        "SET A.CustomField3='Validated' " +
                                        "WHERE A.Remark = @JobId";

                                updateCmd.Parameters.AddWithValue("@JobId".ToUpper(), ll.PHJBID.Trim());
                                Db2GetDataTable(updateCmd, Db2GudangSysConnection());
                            }

                            if (ll.PHSTAT.Trim() == "X")
                            {
                                iDB2Command updateCmd = new iDB2Command();

                                if (ll.PHCASE.Trim() == "")
                                {
                                    updateCmd.CommandText = "UPDATE " + GudangSysSchema + ".RcvOrderHeaders " +
                                            "SET StatusCode= 'C', Updatedon=@Updatedon WHERE TenantCode = @TenantCode AND OrderNumber =@OrderNumber ";

                                    updateCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
                                    updateCmd.Parameters.AddWithValue("@OrderNumber".ToUpper(), ll.PHINVN.Trim());
                                    updateCmd.Parameters.AddWithValue("@Updatedon", datestamp);
                                    Db2GetDataTable(updateCmd, Db2GudangSysConnection());
                                }
                                updateCmd = new iDB2Command();
                                updateCmd.CommandText = "UPDATE " + GudangSysSchema + ".RcvOrderDetails A " +
                                        "SET  A.CustomField3='Confirmed' " +
                                        "WHERE A.Remark = @JobId";

                                updateCmd.Parameters.AddWithValue("@JobId".ToUpper(), ll.PHJBID.Trim());
                                Db2GetDataTable(updateCmd, Db2GudangSysConnection());

                                updateCmd = new iDB2Command();
                                updateCmd.CommandText = "UPDATE " + TopasSchema + ".P1BP2PHP SET PHSTAT='C' " +
                                "WHERE PHJBID ='" + ll.PHJBID.Trim() + "'";
                                Db2GetDataTable(updateCmd, Db2TopasConnection());

                                //Temporary remarked until stabilise
                                //updateCmd = new iDB2Command();
                                //updateCmd.CommandText = "SELECT rod.OrderNumber,rod.ItemCode,rod.ItemUomCode,rod.BatchNumber,rod.CheckLeQty,rod.BinLeQty " +
                                //    "FROM " + GudangSysSchema + ".RcvOrderDetails AS rod WHERE (rod.CheckLeQty>0 OR rod.BinLeQty>0) " +
                                //    "AND rod.OrderNumber = '" + ll.PHINVN.Trim() + "'";
                                //Db2GetDataTable(updateCmd, Db2TopasConnection());

                                //gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                                //List<RCV> RCV = new List<RCV>();
                                //RCV = ConvertDataTable<RCV>(gudangsysdata);

                                //if (RCV.Count() > 0)
                                //{
                                //    ExecuteCommand(ConfigurationManager.AppSettings["Block_Command"].ToString() + "BLOCK " + ll.PHINVN);
                                //}

                            }
                        }



                    }

                    if (ProcessingMode.IndexOf("Shipping") != -1 || ProcessingMode.ToString() == "")
                    {
                        //Exports 
                        //Console.WriteLine(DateTime.Now.ToString() + " : Sending Exports...");
                        //GSysSQL = "select * from " + GudangSysSchema + ".SalesOrderHeaders WHERE StatusCode = 'C' AND OrderTypeCode LIKE 'EX%' ";
                        //iP1BCEXIP(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);

                        //Check for errors
                        iDB2Command sqlCmd = new iDB2Command();
                        sqlCmd.CommandText = "SELECT * from " + TopasSchema + ".P1BCEXIP where EHFLAG in ('F','X') ";
                        DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                        List<P1BCEXIP> P1BCEXIP = new List<P1BCEXIP>();
                        P1BCEXIP = ConvertDataTable<P1BCEXIP>(gudangsysdata);

                        foreach (var ll in P1BCEXIP)
                        {
                            if (ll.EHFLAG.Trim() == "F")
                            {
                                iDB2Command updateCmd1 = new iDB2Command();
                                updateCmd1.CommandText = "UPDATE " + GudangSysSchema + ".SalesRouteDelivery A " +
                                        "SET A.Reference='Error' " +
                                        "WHERE A.DeliveryNo = '" + ll.EHJBID.Trim() + "'";

                                Db2GetDataTable(updateCmd1, Db2GudangSysConnection());

                            }

                            if (ll.EHFLAG.Trim() == "X")
                            {
                                iDB2Command updateCmd1 = new iDB2Command();
                                updateCmd1.CommandText = "UPDATE " + GudangSysSchema + ".SalesRouteDelivery A " +
                                        "SET A.Reference='Completed', Status = 'Closed'" +
                                        "WHERE A.DeliveryNo = '" + ll.EHJBID.Trim() + "'";

                                Db2GetDataTable(updateCmd1, Db2GudangSysConnection());

                                updateCmd1 = new iDB2Command();
                                updateCmd1.CommandText = "select * from " + TopasSchema + ".P1CFIVP  " +
                                        "WHERE CIFLAG = '' AND CISORC <> 'TPL' AND";

                                //ExecuteCommand(ConfigurationManager.AppSettings["Block_Command"].ToString() + "BLOCK2 " + ll.EHJBID.Trim());
                            }
                        }


                        Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        //Exports

                        //Domestic 
                        Console.WriteLine(DateTime.Now.ToString() + " : Sending Domestic/EM...");
                        GSysSQL = "select * from " + GudangSysSchema + ".SalesOrderHeaders WHERE StatusCode = 'C'  ";
                        iP1BCCHP(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                        iP1BCCHP_P2P(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);
                        //Check for errors
                        sqlCmd = new iDB2Command();
                        sqlCmd.CommandText = "SELECT * from " + TopasSchema + ".P1BCCHP where CHFLAG in ('F','X') ";
                        gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                        List<P1BCCHP> P1BCCHP = new List<P1BCCHP>();
                        P1BCCHP = ConvertDataTable<P1BCCHP>(gudangsysdata);

                        foreach (var ll in P1BCCHP)
                        {
                            if (ll.CHFLAG.Trim() == "F")
                            {
                                iDB2Command updateCmd1 = new iDB2Command();
                                updateCmd1.CommandText = "UPDATE " + GudangSysSchema + ".SalesRouteDelivery A " +
                                        "SET A.Reference='Error' " +
                                        "WHERE A.DeliveryNo = '" + ll.CHJBID.Trim() + "'";

                                Db2GetDataTable(updateCmd1, Db2GudangSysConnection());

                            }

                            if (ll.CHFLAG.Trim() == "X")
                            {
                                iDB2Command updateCmd1 = new iDB2Command();
                                updateCmd1.CommandText = "UPDATE " + GudangSysSchema + ".SalesRouteDelivery A " +
                                        "SET A.Reference='Completed', A.Status='Closed'  " +
                                        "WHERE A.DeliveryNo = '" + ll.CHJBID.Trim() + "'";

                                Db2GetDataTable(updateCmd1, Db2GudangSysConnection());

                                updateCmd1 = new iDB2Command();
                                updateCmd1.CommandText = "UPDATE " + GudangSysSchema + ".CaseLabelRef A " +
                                        "SET A.Status='Shipped' " +
                                        "WHERE A.CaseNumber IN ( select CaseNumber from " + GudangSysSchema + ".CaseRef " +
                                        "WHERE DeliveryNo='" + ll.CHJBID.Trim() + "'  )";

                                Db2GetDataTable(updateCmd1, Db2GudangSysConnection());

                                updateCmd1 = new iDB2Command();
                                updateCmd1.CommandText = "UPDATE " + TopasSchema + ".P1BCCHP A " +
                                        "SET A.CHFLAG='C' " +
                                        "WHERE A.CHJBID = '" + ll.CHJBID.Trim() + "'";

                                Db2GetDataTable(updateCmd1, Db2GudangSysConnection());

                                // ExecuteCommand(ConfigurationManager.AppSettings["Block_Command"].ToString() + "BLOCK2 " + ll.CHJBID.Trim());
                            }
                        }

                        iDB2Command updateCmd = new iDB2Command();
                        updateCmd.CommandText = "select CIJBID,CISORC,CIFLAG,CIINVN,CIUPDT,CIUPTM from " + TopasSchema + ".P1CFIVP  " +
                                "WHERE CIFLAG = '' AND CISORC <> 'TPL' ";
                        gudangsysdata = Db2GetDataTable(updateCmd, Db2GudangSysConnection());

                        List<P1CFIVP> P1CFIVP = new List<P1CFIVP>();
                        P1CFIVP = ConvertDataTable<P1CFIVP>(gudangsysdata);
                        foreach (var i in P1CFIVP)
                        {
                            iDB2Command updateCmd1 = new iDB2Command();
                            updateCmd1.CommandText = "update  " + GudangSysSchema + ".SalesRouteDelivery " +
                                "set VehicleNo1='" + i.CIINVN.Trim() + "', Status='Closed'  " +
                                    "WHERE DeliveryNo = '" + i.CIJBID.Trim() + "'";
                            Db2GetDataTable(updateCmd1, Db2GudangSysConnection());

                            
                            updateCmd1 = new iDB2Command();
                            updateCmd1.CommandText = "update  " + TopasSchema + ".P1CFIVP set CIFLAG='X'  " +
                                    "WHERE CIJBID = '" + i.CIJBID.Trim() + "'";
                            Db2GetDataTable(updateCmd1, Db2GudangSysConnection());

                            Console.WriteLine();
                            updateCmd1 = new iDB2Command();
                            updateCmd1.CommandText = "update " + GudangSysSchema + ".salesorderdetails set customfield1 = '" + i.CIJBID.Trim() + "' " +
                                "where batchnumber in (select casenumber from " + GudangSysSchema + ".caseref where deliveryno = '" + i.CIJBID.Trim() + "') and customfield1 is null";
                            Db2GetDataTable(updateCmd1, Db2GudangSysConnection());
                            
                        }

                        Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        //Domestic
                    }

                    if (ProcessingMode.IndexOf("Location") != -1 || ProcessingMode.ToString() == "")
                    {
                        //Location Assignment 
                        Console.WriteLine(DateTime.Now.ToString() + " : Sending Location Assignment...");

                        iDB2Command sqlCmd = new iDB2Command();
                        sqlCmd.CommandText = "SELECT  Requestid,LocationCode,LotNumber,ItemCode,LocationCode," +
                            "OldNoOfBin,NoOfBin,OldStandardQuantity," +
                            " StandardQuantity,Jobid " +
                            "FROM " + GudangSysSchema + ".LocationAssignment " +
                            "WHERE Status = 'Submit_Cancel' ";
                        DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                        List<LocationAssignment> gudangsyslist = new List<LocationAssignment>();
                        gudangsyslist = ConvertDataTable<LocationAssignment>(gudangsysdata);
                     
                        foreach (LocationAssignment row in gudangsyslist)
                        {
                            sqlCmd = new iDB2Command();
                            sqlCmd.CommandText = "DELETE " + TopasSchema + ".P1LOC2P WHERE L2JBID = '" + row.JOBID + "'";
                            Console.WriteLine(sqlCmd.CommandText);
                            Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                            sqlCmd = new iDB2Command();
                            sqlCmd.CommandText = "UPDATE " + GudangSysSchema + ".LocationAssignment SET " +
                                "Status='Cancelled' where requestid ='" + row.REQUESTID + "'";
                            Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                        }
                        
                        iDB2Command updateCmdMain = new iDB2Command();
                        updateCmdMain.CommandText = "select itemcode,lotnumber,min(tolocationcode) as tolocationcode," +
                            "min(fromlocationcode)  as FROMLOCATIONCODE, " +
                            "count(ITEMTRANSFERID) as ArrowBinLocation, SUM(BINOKQTY) as BINOKQTY, updatedby " +
                            "from " + GudangSysSchema + ".ItemTransferheaders " +
                            "where (fromlocationCode = '88888888' or (DefaultLocation =1) ) and statuscode = 'C' " +
                            "and tolocationCode  not in (select locationcode from  " + GudangSysSchema + ".itemrecommendedwhlocationref ) " +
                            "AND tolocationCode in (select XXLOC from " + TopasSchema + ".P2ELOCP where XXRACK = '0' ) " +
                            "group by itemcode,lotnumber, updatedby";

                        updateCmdMain.CommandTimeout = 0;
                        DataTable gudangsysdataMain = Db2GetDataTable(updateCmdMain, Db2GudangSysConnection());
                        //ItemTransferHeader
                        List<ItemTransferHeader> ItemTransferHeader = new List<ItemTransferHeader>();
                        ItemTransferHeader = ConvertDataTable<ItemTransferHeader>(gudangsysdataMain);
                        foreach (var tt in ItemTransferHeader)
                        {
                            string arrowbinlocation = "N";
                            if (tt.ARROWBINLOCATION > 1) { arrowbinlocation = "Y"; }

                            //get old bin location
                            sqlCmd = new iDB2Command();
                            sqlCmd.CommandText = "SELECT  Requestid,LotNumber,ItemCode,OldNoOfBin,NoOfBin,OldStandardQuantity," +
                                " StandardQuantity,Jobid " +
                                "FROM " + GudangSysSchema + ".LocationAssignment " +
                                "WHERE Status = 'Completed' and ItemCode='" + tt.ITEMCODE + "' and   order by Requestid desc ";
                            DataTable gudangsysdata_1 = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                            List<LocationAssignment> gudangsyslist_1 = new List<LocationAssignment>();
                            gudangsyslist_1 = ConvertDataTable<LocationAssignment>(gudangsysdata_1);
                            string oldnoofbin = "1";
                            string oldstdqty = "1";
                            foreach (LocationAssignment row1 in gudangsyslist_1)
                            {
                                oldnoofbin = row1.NOOFBIN.ToString();
                                oldstdqty = row1.STANDARDQUANTITY.ToString();
                                break;
                            }


                            iDB2Command updateCmd = new iDB2Command();
                            updateCmd.CommandText = "INSERT INTO " + GudangSysSchema + ".LocationAssignment  " +
                                "(JobId, ItemCode, LotNumber, LocationCode, ZoneCode, OldNoOfBin, NoOfBin, OldLocation, " +
                                "OldStandardQuantity, " +
                                "StandardQuantity, ArrowBinLocation, CreatedOn, CreatedBy, UpdatedBy,UpdatedOn,Status, " +
                                "Remarks, Custom1, Custom2, Custom3, Custom4) VALUES " +
                                    "('', '" + tt.ITEMCODE + "', '" + tt.LOTNUMBER + "', '" + tt.TOLOCATIONCODE + "', " +
                                    "'" + tt.TOLOCATIONCODE.Substring(0, 2) + "', '" + oldnoofbin + "', " +
                                    "'" + tt.ARROWBINLOCATION.ToString() + "', '" + tt.FROMLOCATIONCODE + "', '" + oldstdqty + "', " +
                                    "'" + tt.BINOKQTY.ToString() + "', " +
                                    "'" + arrowbinlocation + "', @DateStamp, '" + tt.UPDATEDBY + "', 'TBIPS',@DateStamp1,	'Submitted', 'Transfer', " +
                                    "'', '', '', '') ";
                            updateCmd.Parameters.AddWithValue("@DateStamp", DateTime.Now);
                            updateCmd.Parameters.AddWithValue("@DateStamp1", DateTime.Now);
                            Console.WriteLine(updateCmd.CommandText);
                            Db2GetDataTable(updateCmd, Db2GudangSysConnection());

                            updateCmd = new iDB2Command();
                            updateCmd.CommandText = "UPDATE " + GudangSysSchema + ".LocationAssignment " +
                                "SET JOBID = CONCAT('" + TopasDateString + "', cast(RequestId as varchar(5))) " +
                                "WHERE JOBID ='' AND STATUS = 'Submitted'";
                            updateCmd.Parameters.AddWithValue("@DateStamp", DateTime.Now);
                            Db2GetDataTable(updateCmd, Db2GudangSysConnection());


                            updateCmd = new iDB2Command();
                            updateCmd.CommandText = "UPDATE " + GudangSysSchema + ".ItemTransferheaders " +
                                "SET StatusCode ='S' WHERE ItemCode = '" + tt.ITEMCODE.ToString() + "' and statuscode = 'C' ";
                            updateCmd.Parameters.AddWithValue("@DateStamp", DateTime.Now);
                            Db2GetDataTable(updateCmd, Db2GudangSysConnection());

                        }

                       
                        GSysSQL = "select * from " + GudangSysSchema + ".LocationAssignment WHERE Status = 'Submitted' and Jobid <> '' ";
                        iP1LOC2P(TopasSchema, GudangSysSchema, ClientAccount, TopasSQL, GSysSQL, datestamp, WarehouseCode);



                        //Check for errors
                        sqlCmd = new iDB2Command();
                        sqlCmd.CommandText = "SELECT * from " + TopasSchema + ".P1LOC2P where L2FLAG in ('F','X') ";
                        gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                        List<P1LOC2P> P1LOC2P = new List<P1LOC2P>();
                        P1LOC2P = ConvertDataTable<P1LOC2P>(gudangsysdata);

                        foreach (var ll in P1LOC2P)
                        {
                            if (ll.L2FLAG.Trim() == "F")
                            {
                                iDB2Command updateCmd = new iDB2Command();
                                updateCmd.CommandText = "UPDATE " + GudangSysSchema + ".LocationAssignment A " +
                                        "SET A.Status='Error', A.Custom1 = '" + ll.L2FPNT + "' " +
                                        "WHERE A.JobId = @JobId";

                                updateCmd.Parameters.AddWithValue("@JobId".ToUpper(), ll.L2JBID.Trim());
                                Db2GetDataTable(updateCmd, Db2GudangSysConnection());

                                updateCmd = new iDB2Command();
                                updateCmd.CommandText = "UPDATE " + TopasSchema + ".P1LOC2P A " +
                                        "SET A.L2FLAG='E' " +
                                        "WHERE A.L2JBID = @JobId";

                                updateCmd.Parameters.AddWithValue("@JobId".ToUpper(), ll.L2JBID.Trim());
                                Db2GetDataTable(updateCmd, Db2GudangSysConnection());

                            }

                            if (ll.L2FLAG.Trim() == "X")
                            {

                                iDB2Command updateCmd = new iDB2Command();
                                updateCmd.CommandText = "UPDATE " + GudangSysSchema + ".LocationAssignment A " +
                                        "SET A.Status='Completed' " +
                                        "WHERE A.JobId = @JobId";

                                updateCmd.Parameters.AddWithValue("@JobId".ToUpper(), ll.L2JBID.Trim());
                                Db2GetDataTable(updateCmd, Db2GudangSysConnection());

                                updateCmd = new iDB2Command();
                                updateCmd.CommandText = "DELETE  " + GudangSysSchema + ".ItemRecommendedWHLocationRef " +
                                    " WHERE TenantCode ='" + ClientAccount + "' AND  ItemCode ='" + ll.L2PNO.Trim() + "'AND WarehouseCode='" + WarehouseCode + "' " +
                                    " AND LocationCode='88888888'";
                                Db2GetDataTable(updateCmd, Db2GudangSysConnection());

                                updateCmd = new iDB2Command();
                                updateCmd.CommandText = "INSERT INTO " + GudangSysSchema + ".ItemRecommendedWHLocationRef " +
                                    "( TenantCode, ItemCode, WarehouseCode, ZoneCode, LocationCode, IsArchived) " +
                                    " VALUES ( '" + ClientAccount + "', '" + ll.L2PNO.Trim() + "', '" + WarehouseCode + "', '', '" + ll.L2NLOC.Trim() + "', 0) ";

                                Db2GetDataTable(updateCmd, Db2GudangSysConnection());

                                updateCmd = new iDB2Command();
                                updateCmd.CommandText = "UPDATE " + TopasSchema + ".P1LOC2P " +
                                        "SET L2FLAG='C' " +
                                        "WHERE L2JBID = @JobId";

                                updateCmd.Parameters.AddWithValue("@JobId".ToUpper(), ll.L2JBID.Trim());
                                Db2GetDataTable(updateCmd, Db2GudangSysConnection());

                            }
                        }

                        Console.WriteLine(DateTime.Now.ToString() + " - Completed");
                        //Location Assignment


                    }

                    if (ProcessingMode.IndexOf("Stock") != -1 || ProcessingMode.ToString() == "")
                    {

                    }
                }

                if (Mode == "TestFtp")
                {
                    Console.WriteLine("Testing FTP....");
                }
            }
            catch (Exception e)
            {
                var st = new StackTrace(e, true);
                // Get the top stack frame
                var frame = st.GetFrame(st.FrameCount - 1);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("Error [" + DateTime.Now.ToString() + "] : " + e.Message);
                Console.WriteLine("Line Number [" + DateTime.Now.ToString() + "] : " + line.ToString());
                Console.WriteLine("Inner Exception: " + e.InnerException == null ? e.InnerException.ToString() : "");
                Console.ForegroundColor = ConsoleColor.White;
            }


        }

        private static void iP1CSUPP(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {

            Console.Write("Merging Supplier Master");
            iDB2Command updateCmd = new iDB2Command();
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".SupplierMaster destination " +
                "Using (select * from " + TopasSchema + ".P1CSUPP) origin " +
                "ON ( @TenantCode1  = destination.TenantCode AND TRIM(origin.TSVEND) = destination.SupplierCode) " +
                "WHEN MATCHED THEN UPDATE SET " +
                "SupplierName = TRIM(origin.TSSNAM), Address=TRIM(origin.TSADR1) CONCAT TRIM(origin.TSADR2) CONCAT TRIM(origin.TSADR3), " +
                " UpdatedOn= @UpdatedOn1 " +
                "WHEN NOT MATCHED THEN " +
                "INSERT " +
                "(TenantCode, SupplierCode, SupplierName, Address, CreatedBy, " +
                "CreatedOn, UpdatedBy, UpdatedOn, IsArchived, SupplierTypeCode, ContactPerson, " +
                "ContactNumber, EmailAddress, FaxNumber)" +
                //"FromTime, ToTime) " +
                "VALUES " +
                "(@TenantCode, TRIM(origin.TSVEND), TRIM(origin.TSSNAM), TRIM(origin.TSADR1) CONCAT TRIM(origin.TSADR2) CONCAT TRIM(origin.TSADR3), @CreatedBy, @CreatedOn, @UpdatedBy, " +
                "@UpdatedOn, 0, 'DEFAULT', '', '', '', '')";
            //"TRIM(origin.TSDTFR), TRIM(origin.TSDTTO))";
            updateCmd.Parameters.AddWithValue("@TenantCode1".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedOn1".ToUpper(), datestamp);

            Db2GetDataTable(updateCmd, Db2GudangSysConnection());


        }

        private static void iREFERP(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {

            Console.Write("Merging Denial Reason");
            iDB2Command updateCmd = new iDB2Command();
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".DenialReason destination " +
                "Using (select * from " + TopasSchema + ".REFERP where REFCAT = '834' and REFKEY NOT like 'R%' ) origin " +
                "ON (  TRIM(origin.REFKEY) = destination.Reason) " +
                //"WHEN MATCHED THEN UPDATE SET " +
                //"Description = TRIM(origin.REFDAT) " +
                "WHEN NOT MATCHED THEN " +
                "INSERT " +
                "( Reason, Description, CreatedBy, " +
                "CreatedOn, UpdatedBy, UpdatedOn, IsArchived, Type, ResultType) " +
                "VALUES " +
                "( TRIM(origin.REFKEY),TRIM(origin.REFDAT),   @CreatedBy, @CreatedOn, @UpdatedBy, " +
                "@UpdatedOn, 0, 'Receiving Checking', 'Le')";

            updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedOn1".ToUpper(), datestamp);

            Db2GetDataTable(updateCmd, Db2GudangSysConnection());

            updateCmd = new iDB2Command();
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".DenialReason destination " +
                "Using (select * from " + TopasSchema + ".REFERP where REFCAT = '834' and REFKEY like 'R%') origin " +
                "ON ( TRIM(origin.REFKEY) = destination.Reason) " +
                //"WHEN MATCHED THEN UPDATE SET " +
                //"Description = TRIM(origin.REFDAT) " +
                "WHEN NOT MATCHED THEN " +
                "INSERT " +
                "(Reason, Description, CreatedBy, " +
                "CreatedOn, UpdatedBy, UpdatedOn, IsArchived, Type, ResultType) " +
                "VALUES " +
                "(TRIM(origin.REFKEY),TRIM(origin.REFDAT), @CreatedBy, @CreatedOn, @UpdatedBy, " +
                "@UpdatedOn, 0, 'Shipping Picking', 'Le')";

            updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedOn1".ToUpper(), datestamp);

            Db2GetDataTable(updateCmd, Db2GudangSysConnection());

        }

        private static void iP1GIM0P(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {

            //P1GIM0P
            Console.WriteLine("Merging Client Account Master");
            iDB2Command updateCmd = new iDB2Command();
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".TenantMaster destination " +
                "Using (select * from " + TopasSchema + ".P1GIM0P) origin " +
                "ON (trim(origin.G0FLID) = 'C') " +
                "WHEN MATCHED THEN UPDATE SET " +
                "TenantName = TRIM(origin.G0CONM), Address=TRIM(origin.G0ADR1) CONCAT TRIM(origin.G0ADR2) CONCAT TRIM(origin.G0ADR3)," +
                "UpdatedOn= @UpdatedOn1,ContactPerson='' " +
                "WHEN NOT MATCHED THEN " +
                "INSERT " +
                "(TenantCode, TenantName, Address, CreatedBy, " +
                "CreatedOn, UpdatedBy, UpdatedOn, IsArchived,ContactPerson) " +
                "VALUES " +
                "(@TenantCode, TRIM(origin.G0CONM), TRIM(origin.G0ADR1) CONCAT TRIM(origin.G0ADR2) CONCAT TRIM(origin.G0ADR3), @CreatedBy, @CreatedOn, @UpdatedBy, " +
                "@UpdatedOn, 0,'')";
            updateCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedOn1".ToUpper(), datestamp);
            Db2GetDataTable(updateCmd, Db2GudangSysConnection());
            Console.WriteLine(DateTime.Now.ToString() + " - Completed");

            Console.WriteLine("Merging Contract Master");
            updateCmd = new iDB2Command();
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".BillingMaster destination " +
                "Using (select * from " + TopasSchema + ".P1GIM0P) origin " +
                "ON (trim(origin.G0FLID) = 'C' AND destination.TenantCode = '" + ClientAccount + "') " +
                "WHEN NOT MATCHED THEN " +
                "INSERT " +
                "(ContractNo, TenantCode, Currency, BillingCycle, CutOffDate, " +
                "StartDate, EndDate, Status, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn, IsArchived) " +
                "VALUES " +
                "(@TenantCode1,@TenantCode, '', '','',@StartDate,@EndDate,'Active', @CreatedBy, @CreatedOn, @UpdatedBy, " +
                "@UpdatedOn, 0)";

            updateCmd.Parameters.AddWithValue("@TenantCode1".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@StartDate".ToUpper(), datestamp.ToString("yyyy-MM-dd"));
            updateCmd.Parameters.AddWithValue("@EndDate".ToUpper(), datestamp.ToString("yyyy-MM-dd"));
            updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);

            Db2GetDataTable(updateCmd, Db2GudangSysConnection());
            Console.WriteLine(DateTime.Now.ToString() + " - Completed");
            //end
        }

        private static void iP1CUSTP(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {
            Console.WriteLine("Merging Customer Master");
            iDB2Command updateCmd = new iDB2Command();
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".CustomerMaster destination " +
                "Using (select * from " + TopasSchema + ".P1CUSTP) origin " +
                "ON (@TenantCode1  = destination.TenantCode AND TRIM(origin.CMCMNO) = destination.CustomerCode) " +
                "WHEN MATCHED THEN UPDATE SET " +
                "CustomerName = TRIM(origin.CMCMEN), " +
                "ShippingAddress = TRIM(origin.CMADR1) CONCAT TRIM(origin.CMADR2), " +
                "BillingAddress = TRIM(origin.CMADR1) CONCAT TRIM(origin.CMADR2), " +
                "UpdatedOn= @UpdatedOn1,ContactPerson='' " +
                "WHEN NOT MATCHED THEN " +
                "INSERT " +
                "(TenantCode,  CustomerCode,  CustomerName,  InterfaceCustomerCode,  " +
                "CustomerTypeCode,  ShippingAddress,  BillToShippingAddress,  BillingAddress,  ContactPerson,  ContactNumber,  FaxNumber,  " +
                "Email,  SalesPersonInCharge,  CreatedBy,  CreatedOn,  UpdatedBy,  UpdatedOn,  IsArchived,  Status  ,  DefaultAllocation) " +
                "VALUES " +
                "(@TenantCode, TRIM(origin.CMCMNO), TRIM(origin.CMCMEN), TRIM(origin.CMCMNO),'',  TRIM(origin.CMADR1) CONCAT TRIM(origin.CMADR2),'1', " +
                "TRIM(origin.CMADR1) CONCAT TRIM(origin.CMADR2), " +
                "@ContactPerson,@ContactNumber,@FaxNumber,@Email,@SalesPersonInCharge,@CreatedBy, @CreatedOn, @UpdatedBy, " +
                "@UpdatedOn, 0, 'Active', @DefaultAllocation)";
            updateCmd.Parameters.AddWithValue("@TenantCode1".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@ContactPerson".ToUpper(), "");
            updateCmd.Parameters.AddWithValue("@ContactNumber".ToUpper(), "");
            updateCmd.Parameters.AddWithValue("@FaxNumber".ToUpper(), "");
            updateCmd.Parameters.AddWithValue("@Email".ToUpper(), "");
            updateCmd.Parameters.AddWithValue("@SalesPersonInCharge".ToUpper(), "");
            updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedOn1".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@DefaultAllocation".ToUpper(), ConfigurationManager.AppSettings["DefaultAllocation"].ToString());

            Db2GetDataTable(updateCmd, Db2GudangSysConnection());
            Console.WriteLine(DateTime.Now.ToString() + " - Completed");

            Console.WriteLine("Merging Delivery Address Customer");
            updateCmd = new iDB2Command();
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".DeliveryAddress destination " +
                "Using (select * from " + TopasSchema + ".P1CUSTP) origin " +
                "ON (@TenantCode1  = destination.TenantCode AND TRIM(origin.CMCMNO) = destination.CustomerCode) " +
                "WHEN MATCHED AND destination.AddressName='Main' THEN UPDATE SET " +
                "Address = TRIM(origin.CMADR1) CONCAT TRIM(origin.CMADR2), " +
                "UpdatedOn= @UpdatedOn1 " +
                "WHEN NOT MATCHED THEN " +
                "INSERT " +
                "(TenantCode, CustomerCode, AddressName, Address, " +
                "isArchived, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn, PrimaryDeliveryAddress) " +
                "VALUES " +
                "(@TenantCode, TRIM(origin.CMCMNO), @AddressName, TRIM(origin.CMADR1) CONCAT TRIM(origin.CMADR2), " +
                "'0', @CreatedBy, @CreatedOn, @UpdatedBy, @UpdatedOn, '1')";
            updateCmd.Parameters.AddWithValue("@TenantCode1".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@AddressName".ToUpper(), "Main");
            updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedOn1".ToUpper(), datestamp);

            Db2GetDataTable(updateCmd, Db2GudangSysConnection());
            Console.WriteLine(DateTime.Now.ToString() + " - Completed");


            Console.WriteLine("Merging Route Master");
            updateCmd = new iDB2Command();
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".DeliveryChargesRoute destination " +
                "Using (select DISTINCT trim(RURUNO) as RURUNO,trim(RURRNM) as RURRNM from " + TopasSchema + ".P1ROUTP) origin " +
                "ON (@TenantCode1  = destination.TenantCode AND @TenantCode2  = destination.ContractNo  AND @WarehouseCode  = destination.WarehouseCode " +
                "AND TRIM(origin.RURUNO) = destination.Route) " +
                "WHEN MATCHED THEN UPDATE SET " +
                "Description = TRIM(origin.RURRNM) , " +
                "UpdatedOn= @UpdatedOn1 " +
                "WHEN NOT MATCHED THEN " +
                "INSERT " +
                "(TenantCode,ContractNo, WarehouseCode, GLCode, Route, Description, OrderCutOffTime, ScheduledDeparttureTime, " +
                "IsArchived, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn, IsAutoAllocate) " +
                "VALUES " +
                "(@TenantCode, @TenantCode3, @TenantCode4, '', trim(origin.RURUNO), trim(origin.RURRNM),@OrderCutOffTime,'0', " +
                "'0', @CreatedBy, @CreatedOn, @UpdatedBy, @UpdatedOn, '1')";
            updateCmd.Parameters.AddWithValue("@TenantCode1".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@TenantCode2".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@TenantCode3".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@TenantCode4".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@WarehouseCode".ToUpper(), WarehouseCode);
            updateCmd.Parameters.AddWithValue("@OrderCutOffTime".ToUpper(), datestamp.ToString("HH:mm:ss"));
            updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedOn1".ToUpper(), datestamp);

            Db2GetDataTable(updateCmd, Db2GudangSysConnection());
            Console.WriteLine(DateTime.Now.ToString() + " - Completed");


            Console.WriteLine("Merging Route Details");
            updateCmd = new iDB2Command();
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".DeliveryRouteCustomer destination " +
                "Using (" +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA001) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA001 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA002) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA002 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA003) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA003 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA004) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA004 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA005) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA005 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA006) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA006 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA007) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA007 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA008) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA008 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA009) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA009 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA010) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA010 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA011) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA011 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA012) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA012 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA013) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA013 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA014) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA014 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA015) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA015 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA016) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA016 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA017) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA017 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA018) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA018 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA019) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA019 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA020) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA020 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA021) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA021 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA022) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA022 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA023) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA023 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA024) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA024 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA025) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA025 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA026) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA026 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA027) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA027 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA028) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA028 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA029) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA029 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA030) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA030 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA031) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA031 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA032) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA032 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA033) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA033 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA034) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA034 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA035) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA035 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA036) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA036 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA037) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA037 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA038) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA038 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA039) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA039 <> '' UNION ALL " +
                "SELECT DISTINCT TRIM(RURUNO) AS RURUNO,TRIM(RUA040) as CUSTCODE FROM " + TopasSchema + ".P1ROUTP WHERE RUA040 <> '' " +
                ") origin " +
                "ON (@TenantCode1  = destination.TenantCode  AND TRIM(origin.CUSTCODE)  = destination.CustomerCode " +
                "AND TRIM(origin.RURUNO) = destination.UpdatedBy) " +
                "WHEN NOT MATCHED THEN " +
                "INSERT " +
                "(refid, TenantCode, CustomerCode, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn, IsArchived, Status) " +
                "VALUES " +
                "('0', @TenantCode, trim(origin.CUSTCODE), @CreatedBy, @CreatedOn,TRIM(origin.RURUNO), @UpdatedOn, '0', 'Active')";
            updateCmd.Parameters.AddWithValue("@TenantCode1".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@TenantCode2".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);

            Db2GetDataTable(updateCmd, Db2GudangSysConnection());
            updateCmd = new iDB2Command();

            updateCmd.CommandText = "UPDATE destination set " +
                "" + GudangSysSchema + ".DeliveryRouteCustomer";

            Console.WriteLine("Merging Route Refid");
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".DeliveryRouteCustomer destination " +
                "Using (select DISTINCT id, TenantCode,Route from " + GudangSysSchema + ".DeliveryChargesRoute) origin " +
                "ON (origin.TenantCode  = destination.TenantCode AND origin.Route  = destination.UpdatedBy)  " +
                "WHEN MATCHED THEN UPDATE SET " +
                "refid = origin.id ";
            Db2GetDataTable(updateCmd, Db2GudangSysConnection());

            //updateCmd = new iDB2Command();
            //updateCmd.CommandText = "update " + GudangSysSchema + ".DeliveryRouteCustomer set " +
            //    " UpdatedBy = CreatedBy";
            //Db2GetDataTable(updateCmd, Db2GudangSysConnection());
            Console.WriteLine(DateTime.Now.ToString() + " - Completed");
            UpdateTopasStatus("CUSTOMER MASTER");
            UpdateTopasStatus("NIGHT BATCH");
            //end
        }

        private static void iP2ELOCP(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {


            Console.Write("Merging Bin Type");
            iDB2Command updateCmd = new iDB2Command();
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".WHBinTypeRef destination " +
                "Using (select distinct TRIM(XXBINT) as XXBINT from " + TopasSchema + ".P2ELOCP where  trim(XXBINT) <> '') origin " +
                "ON (trim(origin.XXBINT) = destination.TypeCode AND @WarehouseCode =  destination.WarehouseCode) " +
                "WHEN MATCHED THEN UPDATE SET " +
                "Description = TRIM(origin.XXBINT)," +
                "UpdatedOn= @UpdatedOn1 " +
                "WHEN NOT MATCHED THEN " +
                "INSERT " +
                "(WarehouseCode, TypeCode, Description, CreatedBy, " +
                "CreatedOn, UpdatedBy, UpdatedOn, IsArchived, " +
                "Height, Width, Depth,EffectiveVolumePct, ColourCode, BinWeight, ColumnWeight,PalletCapacity) " +
                "VALUES " +
                "(@WarehouseCode1, TRIM(origin.XXBINT), TRIM(origin.XXBINT), @CreatedBy, @CreatedOn, @UpdatedBy, " +
                "@UpdatedOn, 0,'0','0','0','100','#EAEAEA','0','0','100')";


            updateCmd.Parameters.AddWithValue("@WarehouseCode".ToUpper(), WarehouseCode);
            updateCmd.Parameters.AddWithValue("@WarehouseCode1".ToUpper(), WarehouseCode);
            updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedOn1".ToUpper(), datestamp);
            Db2GetDataTable(updateCmd, Db2GudangSysConnection());
            Console.WriteLine(DateTime.Now.ToString() + " - Completed");

            //Console.Write("Merging Zone Type");
            //updateCmd = new iDB2Command();
            //updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".WHZoneTypeRef destination " +
            //    "Using (select distinct LEFT(TRIM(XXLOC),2) as XXZONE from " + TopasSchema + ".P2ELOCP where trim(XXLOC) <> '' " +
            //    "UNION ALL SELECT '88' AS XXZONE FROM " + TopasSchema + ".P2ELOCP where XXLOC= 'A102011A' ) origin " +
            //    "ON (trim(origin.XXZONE) = destination.TypeCode AND @WarehouseCode =  destination.WarehouseCode) " +
            //    "WHEN MATCHED THEN UPDATE SET " +
            //    "Description = TRIM(origin.XXZONE)," +
            //    "UpdatedOn= @UpdatedOn1 " +
            //    "WHEN NOT MATCHED THEN " +
            //    "INSERT " +
            //    "(WarehouseCode, TypeCode, Description, CreatedBy, " +
            //    "CreatedOn, UpdatedBy, UpdatedOn, IsArchived, " +
            //    "ExcludeStockCount, ExcludePicking, CrossDockFlag,ColourCode) " +
            //    "VALUES " +
            //    "(@WarehouseCode1, TRIM(origin.XXZONE), TRIM(origin.XXZONE), @CreatedBy, @CreatedOn, @UpdatedBy, " +
            //    "@UpdatedOn, 0,'0','0','0','#EAEAEA')";

            //updateCmd.Parameters.AddWithValue("@WarehouseCode".ToUpper(), WarehouseCode);
            //updateCmd.Parameters.AddWithValue("@WarehouseCode1".ToUpper(), WarehouseCode);
            //updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            //updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            //updateCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
            //updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
            //updateCmd.Parameters.AddWithValue("@UpdatedOn1".ToUpper(), datestamp);
            //Db2GetDataTable(updateCmd, Db2GudangSysConnection());
            //Console.WriteLine(DateTime.Now.ToString() + " - Completed");


            Console.Write("Merging Location Master");
            updateCmd = new iDB2Command();
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".WHLocationRef destination " +
                "Using (select distinct TRIM(XXLOC) as XXLOC,TRIM(XXBINT) as XXBINT, LEFT(TRIM(XXLOC),2) as XXZONE from " + TopasSchema + ".P2ELOCP " +
                "where trim(XXLOC) <> ''  " +
                "UNION ALL SELECT '88888888' AS XXLOC, '' AS XXBINT, '88' AS XXZONE FROM " + TopasSchema + ".P2ELOCP where XXLOC= 'A102011A' " +
                ") origin " +
                "ON (trim(origin.XXLOC) = destination.LocationCode AND @WarehouseCode =  destination.WarehouseCode) " +
                "WHEN MATCHED THEN UPDATE SET " +
                "BinTypeCode = TRIM(origin.XXBINT)," +
                "UpdatedOn= @UpdatedOn1 " +
                "WHEN NOT MATCHED THEN " +
                "INSERT " +
                "(WarehouseCode, LocationCode, InterfaceLocationCode, ZoneCode,RackNumber,ColumnNumber, " +
                "LevelNumber, BinNumber,BinTypeCode,IsLocked, " +
                "CreatedBy, CreatedOn, UpdatedBy, UpdatedOn, IsArchived, " +
                "LooseNotAllowed, PickFace, PriorityFlag,PriorityNumber) " +
                "VALUES " +
                "(@WarehouseCode1, TRIM(origin.XXLOC), TRIM(origin.XXLOC), TRIM(origin.XXZONE), @RackNumber,@ColumnNumber,@LevelNumber ,@BinNumber, " +
                "TRIM(origin.XXBINT),'0', @CreatedBy, @CreatedOn, @UpdatedBy, " +
                "@UpdatedOn, 0,'1','0','1','1')";


            updateCmd.Parameters.AddWithValue("@WarehouseCode".ToUpper(), WarehouseCode);
            updateCmd.Parameters.AddWithValue("@WarehouseCode1".ToUpper(), WarehouseCode);
            updateCmd.Parameters.AddWithValue("@RackNumber", "");
            updateCmd.Parameters.AddWithValue("@ColumnNumber", "");
            updateCmd.Parameters.AddWithValue("@LevelNumber", "");
            updateCmd.Parameters.AddWithValue("@BinNumber", "");
            updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedOn1".ToUpper(), datestamp);
            Db2GetDataTable(updateCmd, Db2GudangSysConnection());
            Console.WriteLine(DateTime.Now.ToString() + " - Completed");


            //end
        }

        private static void iP1PARTP(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {

            Console.Write("Merging Item Master");
            iDB2Command updateCmd = new iDB2Command();
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".ItemMaster destination " +
                "Using (select TRIM(PMPNO) as PMPNO, MAX(PMPNME) as PMPNME from " + TopasSchema + ".P1PARTP where  trim(PMPNO) <> '' GROUP BY TRIM(PMPNO)) origin " +
                "ON (trim(origin.PMPNO) = destination.ItemCode AND @TenantCode1 =  destination.TenantCode) " +
                "WHEN MATCHED THEN UPDATE SET " +
                "ItemName = TRIM(origin.PMPNME)," +
                "UpdatedOn= @UpdatedOn1 " +
                "WHEN NOT MATCHED THEN " +
                "INSERT " +
                "(TenantCode, ItemCode, InterfaceItemCode,ItemName, CreatedBy, " +
                "CreatedOn, UpdatedBy, UpdatedOn, IsArchived,SupplierCode,UnitCost,IsKitting," +
                "Weight,ShelfLife,Status,ScanType,OverReceiveThresholdQty,MinimumQuantityPriorityPicking )" +
                "VALUES " +
                "(@TenantCode, TRIM(origin.PMPNO), TRIM(origin.PMPNO),TRIM(origin.PMPNME), @CreatedBy, @CreatedOn, @UpdatedBy, " +
                "@UpdatedOn, 0,@SupplierCode,'0.00','0','0','0','1','Quantity','0','0')";

            updateCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@TenantCode1".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedOn1".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@SupplierCode".ToUpper(), "Default");
            updateCmd.Parameters.AddWithValue("@SupplierCode".ToUpper(), "Default");
            Db2GetDataTable(updateCmd, Db2GudangSysConnection());
            Console.WriteLine(DateTime.Now.ToString() + " - Completed");


            Console.Write("Merging Default Locations");
            updateCmd = new iDB2Command();
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".ItemRecommendedWHLocationRef destination " +
                "Using (select TRIM(PMPNO) as PMPNO,TRIM(PMFRAN) as PMFRAN, MAX(PMLOC) as PMLOC " +
                "from " + TopasSchema + ".P1PMSTP where  trim(PMPNO) <> '' AND trim(PMBRCD)  = @TenantCode2 " +
                "GROUP BY TRIM(PMPNO),TRIM(PMFRAN)  ) origin " +
                "ON (trim(origin.PMPNO) = destination.ItemCode AND trim(origin.PMLOC) = destination.LocationCode " +
                "AND @TenantCode1 =  destination.TenantCode AND trim(origin.PMFRAN) = destination.ZoneCode) " +
                "WHEN NOT MATCHED THEN " +
                "INSERT " +
                "(TenantCode, ItemCode, WarehouseCode,LocationCode,ZoneCode,IsArchived) " +
                "VALUES " +
                "(@TenantCode, TRIM(origin.PMPNO), @WarehouseCode,TRIM(origin.PMLOC),TRIM(origin.PMFRAN),  0)";

            updateCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@TenantCode1".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@TenantCode2".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@WarehouseCode".ToUpper(), WarehouseCode);
            Db2GetDataTable(updateCmd, Db2GudangSysConnection());
            Console.WriteLine(DateTime.Now.ToString() + " - Completed");

            Console.Write("Merging Item UoM");
            updateCmd = new iDB2Command();
            updateCmd.CommandText = "MERGE INTO " + GudangSysSchema + ".ItemUomMaster destination " +
                "Using (select TRIM(PMPNO) as PMPNO, MAX(PMPNME) as PMPNME, MAX(PMWGTP) as PMWGTP, MAX(PMHIGP) as PMHIGP , MAX(PMWIDP) as PMWIDP" +
                ", MAX(PMDEPP) as PMDEPP  " +
                "from " + TopasSchema + ".P1PARTP where  trim(PMPNO) <> '' GROUP BY TRIM(PMPNO)) origin " +
                "ON (trim(origin.PMPNO) = destination.ItemCode AND 'PCS'=  destination.UomCode  AND  @TenantCode1 =  destination.TenantCode) " +
                 "WHEN MATCHED THEN UPDATE SET " +
                //"Weight = TRIM(origin.PMWGTP)," +
                "Height = TRIM(origin.PMHIGP)," +
                "Width = TRIM(origin.PMWIDP)," +
                "Depth = TRIM(origin.PMDEPP)," +
                "UpdatedOn= @UpdatedOn1 " +
                "WHEN NOT MATCHED THEN " +
                "INSERT " +
                "(TenantCode, ItemCode, UomCode,Height, Width, Depth,LooseQty,ItemPrice," +
                "CreatedBy, CreatedOn, UpdatedBy, UpdatedOn,IsArchived) " +
                "VALUES " +
                "(@TenantCode, TRIM(origin.PMPNO), 'PCS',TRIM(origin.PMHIGP), TRIM(origin.PMWIDP), TRIM(origin.PMDEPP),'1','0.00', " +
                "@CreatedBy,@CreatedOn, @UpdatedBy, @UpdatedOn,'0')";

            updateCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@TenantCode1".ToUpper(), ClientAccount);
            updateCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
            updateCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@UpdatedOn1".ToUpper(), datestamp);
            updateCmd.Parameters.AddWithValue("@WarehouseCode".ToUpper(), WarehouseCode);
            Db2GetDataTable(updateCmd, Db2GudangSysConnection());

            Console.WriteLine("Updating ASN primary locations ...");

            updateCmd = new iDB2Command();
            updateCmd.CommandText = " update TBPPRLIB.rcvOrderDetails  a set a.LocationCode =  (select  b.PMLOC " +
            "from TPSPRLIB.P1PMSTP b where trim(b.PMPNO) <> '' AND trim(b.PMBRCD)  = '149' and a.ItemCode = b.PMPNO and a.lotnumber = b.PMFRAN   ) " +
            "where a.crossdockflag = 0 and a.StatusCode = 'N'";
            Db2GetDataTable(updateCmd, Db2GudangSysConnection());

            Console.WriteLine(DateTime.Now.ToString() + " - Completed");

            UpdateTopasStatus("PART MASTER");
        }

        private static void iP1BCBHP(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {
            string flag = "X";
            iDB2Command sqlCmd = new iDB2Command();
            sqlCmd.CommandText = TopasSQL;
            DataTable topasdata = Db2GetDataTable(sqlCmd, Db2TopasConnection());

            sqlCmd = new iDB2Command();
            sqlCmd = new iDB2Command();
            sqlCmd.CommandText = GSysSQL;
            DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());


            List<P1BCBHP> topaslist = new List<P1BCBHP>();
            topaslist = ConvertDataTable<P1BCBHP>(topasdata);

            List<RcvOrderHeaders> gudangsyslist = new List<RcvOrderHeaders>();
            gudangsyslist = ConvertDataTable<RcvOrderHeaders>(gudangsysdata);

            foreach (P1BCBHP row in topaslist)
            {

                int rowcount = Int32.Parse(row.BHNRCD.Trim());

                if (rowcount > 0)
                {
                    if (row.BHINVO.Trim() != "")
                    {
                        sqlCmd = new iDB2Command();
                        sqlCmd.CommandText = "SELECT * FROM " + TopasSchema + ".P1BCBIP WHERE BSBHNO = @BSBHNO";
                        sqlCmd.Parameters.AddWithValue("@BSBHNO", row.BHBHNO.Trim());

                        DataTable P1BCBIPdata = Db2GetDataTable(sqlCmd, Db2TopasConnection());
                        List<P1BCBIP> P1BCBIPlist = new List<P1BCBIP>();
                        P1BCBIPlist = ConvertDataTable<P1BCBIP>(P1BCBIPdata);

                        if (P1BCBIPdata.Rows.Count == rowcount)
                        {
                            int countrecords = 0;
                            foreach (var i in P1BCBIPlist)
                            {
                                if (countrecords == 0)
                                {

                                    string TypeCode = i.BSIMPT.Trim();

                                    if (TypeCode == "1") TypeCode = "TMC";
                                    if (TypeCode == "2") TypeCode = "TMAP";
                                    if (TypeCode == "3") TypeCode = "LSP";

                                    TypeCode = TypeCode + "-" + row.BHSHPT;
                                    sqlCmd = new iDB2Command();
                                    sqlCmd.CommandText = "INSERT INTO " + GudangSysSchema + ".RcvOrderHeaders " +
                                        "(  TenantCode, OrderNumber, SupplierCode, SupplierOrderNumber, " +
                                        "EstimatedDeliveryDate,  ActualDeliveryDate,  TypeCode,  StatusCode, WarehouseCode,  InvoiceNo,  " +
                                        "CreatedBy,  CreatedOn,  UpdatedBy,  " +
                                        "UpdatedOn,  IsArchived,  AutoPrint,  Remark,  SortingFee,  TallyClerkFee,  " +
                                        "OvertimeHours,  OvertimeManpower ) VALUES (" +
                                        "@TenantCode, @OrderNumber, @SupplierCode, @SupplierOrderNumber, " +
                                        "@EstimatedDeliveryDate,  @ActualDeliveryDate,  @TypeCode,  @StatusCode, @WarehouseCode,  @InvoiceNo,  " +
                                        "@CreatedBy,  @CreatedOn,  @UpdatedBy,  " +
                                        "@UpdatedOn,  @IsArchived,  @AutoPrint,  @Remark,  @SortingFee,  @TallyClerkFee, @OvertimeHours,  @OvertimeManpower)";
                                    sqlCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
                                    sqlCmd.Parameters.AddWithValue("@OrderNumber".ToUpper(), i.BSIVNO.Trim());
                                    sqlCmd.Parameters.AddWithValue("@SupplierCode".ToUpper(), row.BHVEND.Trim());
                                    sqlCmd.Parameters.AddWithValue("@SupplierOrderNumber".ToUpper(), row.BHBHNO.Trim());
                                    sqlCmd.Parameters.AddWithValue("@EstimatedDeliveryDate".ToUpper(), datestamp.ToString("yyyy-MM-dd"));
                                    sqlCmd.Parameters.AddWithValue("@ActualDeliveryDate".ToUpper(), DBNull.Value);
                                    sqlCmd.Parameters.AddWithValue("@TypeCode".ToUpper(), TypeCode);
                                    sqlCmd.Parameters.AddWithValue("@StatusCode".ToUpper(), "N");
                                    sqlCmd.Parameters.AddWithValue("@WarehouseCode".ToUpper(), WarehouseCode);
                                    sqlCmd.Parameters.AddWithValue("@InvoiceNo".ToUpper(), i.BSIMPT);
                                    sqlCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
                                    sqlCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
                                    sqlCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
                                    sqlCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
                                    sqlCmd.Parameters.AddWithValue("@IsArchived".ToUpper(), "0");
                                    sqlCmd.Parameters.AddWithValue("@AutoPrint".ToUpper(), "0");
                                    sqlCmd.Parameters.AddWithValue("@Remark".ToUpper(), "");
                                    sqlCmd.Parameters.AddWithValue("@SortingFee".ToUpper(), "0.00");
                                    sqlCmd.Parameters.AddWithValue("@TallyClerkFee".ToUpper(), "0.00");
                                    sqlCmd.Parameters.AddWithValue("@OvertimeHours".ToUpper(), "0");
                                    sqlCmd.Parameters.AddWithValue("@OvertimeManpower".ToUpper(), "0");

                                    Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                                }
                                iDB2Command sqlCmd2 = new iDB2Command();
                                sqlCmd2.CommandText = "INSERT INTO " + GudangSysSchema + ".RcvOrderDetails " +
                                    "( TenantCode,OrderNumber,CaseNumber,ItemCode,ItemUomCode,UomLooseQty,BatchNumber," +
                                    "LotNumber,ExpiryDate,OrderGoQty,CheckOkQty,CheckLeQty,CheckQuQty,CheckOkUnitQty,CheckLeUnitQty,CheckQuUnitQty," +
                                    "BinOkQty,BinLeQty,BinQuQty,BinOkUnitQty,BinLeUnitQty,BinQuUnitQty,StatusCode,StockTxTypeCode,CreatedBy,CreatedOn,UpdatedBy," +
                                    "UpdatedOn,IsArchived,IsCheckItem,Remark,ItemBatchId,LineNumber,LocationCode,ProductionDate,Key1,Key2,CrossDockFlag,CustomerCode, CustomField1) VALUES " +
                                    "( @TenantCode,@OrderNumber,@CaseNumber,@ItemCode,@ItemUomCode,@UomLooseQty,@BatchNumber," +
                                    "@LotNumber,@ExpiryDate,@OrderGoQty,@CheckOkQty,@CheckLeQty,@CheckQuQty,@CheckOkUnitQty,@CheckLeUnitQty,@CheckQuUnitQty," +
                                    "@BinOkQty,@BinLeQty,@BinQuQty,@BinOkUnitQty,@BinLeUnitQty,@BinQuUnitQty,@StatusCode,@StockTxTypeCode,@CreatedBy,@CreatedOn,@UpdatedBy," +
                                    "@UpdatedOn,@IsArchived,@IsCheckItem,@Remark,@ItemBatchId,@LineNumber,@LocationCode,@ProductionDate,@Key1,@Key2,@CrossDockFlag,@CustomerCode,@OrderTyepe )";
                                sqlCmd2.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
                                sqlCmd2.Parameters.AddWithValue("@OrderNumber".ToUpper(), i.BSIVNO.Trim());
                                sqlCmd2.Parameters.AddWithValue("@CaseNumber".ToUpper(), i.BSCASE.Trim());
                                sqlCmd2.Parameters.AddWithValue("@ItemCode".ToUpper(), i.BSPNO.Trim());
                                sqlCmd2.Parameters.AddWithValue("@ItemUomCode".ToUpper(), "PCS");
                                sqlCmd2.Parameters.AddWithValue("@UomLooseQty".ToUpper(), "1");
                                sqlCmd2.Parameters.AddWithValue("@BatchNumber".ToUpper(), "");
                                sqlCmd2.Parameters.AddWithValue("@LotNumber".ToUpper(), i.BSFRAN.Trim());

                                sqlCmd2.Parameters.AddWithValue("@ExpiryDate".ToUpper(), DBNull.Value);
                                sqlCmd2.Parameters.AddWithValue("@OrderGoQty".ToUpper(), i.BSIQTY.Trim());
                                sqlCmd2.Parameters.AddWithValue("@CheckOkQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@CheckLeQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@CheckQuQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@CheckOkUnitQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@CheckLeUnitQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@CheckQuUnitQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@BinOkQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@BinLeQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@BinQuQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@BinOkUnitQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@BinLeUnitQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@BinQuUnitQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@StatusCode".ToUpper(), "N");
                                sqlCmd2.Parameters.AddWithValue("@StockTxTypeCode".ToUpper(), "Av");
                                sqlCmd2.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
                                sqlCmd2.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
                                sqlCmd2.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
                                sqlCmd2.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
                                sqlCmd2.Parameters.AddWithValue("@IsArchived".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@IsCheckItem".ToUpper(), "1");
                                sqlCmd2.Parameters.AddWithValue("@Remark".ToUpper(), "");
                                sqlCmd2.Parameters.AddWithValue("@ItemBatchId".ToUpper(), DBNull.Value);
                                sqlCmd2.Parameters.AddWithValue("@LineNumber".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@LocationCode".ToUpper(), i.BSLOC.Trim());
                                sqlCmd2.Parameters.AddWithValue("@ProductionDate".ToUpper(), DBNull.Value);
                                sqlCmd2.Parameters.AddWithValue("@Key1".ToUpper(), i.BSBHNO.Trim());
                                sqlCmd2.Parameters.AddWithValue("@Key2".ToUpper(), i.BSBITM.Trim());
                                sqlCmd2.Parameters.AddWithValue("@CrossDockFlag".ToUpper(), (i.BSLOC.Trim() == "" && i.BSCUNO.Trim() != "") ? "1" : "0");
                                sqlCmd2.Parameters.AddWithValue("@CustomerCode".ToUpper(), i.BSCUNO.Trim());
                                sqlCmd2.Parameters.AddWithValue("@OrderType".ToUpper(), i.BSODTP.Trim());
                                Db2GetDataTable(sqlCmd2, Db2GudangSysConnection());
                                countrecords++;
                            }
                            flag = "X";
                        }
                        else
                        {
                            flag = "E";
                        }
                        iDB2Command updateCmd = new iDB2Command();

                        updateCmd.CommandText = "UPDATE " + TopasSchema + ".P1BCBHP  SET " +
                            "BHFLAG = @BHFLAG,  BHUPDT= @BHUPDT, BHUPTM=@BHUPTM " +
                            "WHERE BHBHNO = @BHBHNO";
                        updateCmd.Parameters.AddWithValue("@BHBHNO", row.BHBHNO.Trim());
                        updateCmd.Parameters.AddWithValue("@BHUPDT", TopasDate(DateTime.Now));
                        updateCmd.Parameters.AddWithValue("@BHUPTM", TopasTime(DateTime.Now));
                        updateCmd.Parameters.AddWithValue("@BHFLAG", flag);
                        Db2GetDataTable(updateCmd, Db2TopasConnection());

                    }
                }
            }
            //end
        }


        private static void iP1BCEXHP(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {

            string flag = "X";
            var DefaultAllocation = ConfigurationManager.AppSettings["DefaultAllocation"].ToString();

            iDB2Command sqlCmd = new iDB2Command();
            sqlCmd.CommandText = TopasSQL;
            DataTable topasdata = Db2GetDataTable(sqlCmd, Db2TopasConnection());

            sqlCmd = new iDB2Command();
            sqlCmd.CommandText = GSysSQL;
            DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

            List<P1BCEXHP> topaslist = new List<P1BCEXHP>();
            topaslist = ConvertDataTable<P1BCEXHP>(topasdata);

            List<SalesOrderHeaders> gudangsyslist = new List<SalesOrderHeaders>();
            gudangsyslist = ConvertDataTable<SalesOrderHeaders>(gudangsysdata);

            foreach (P1BCEXHP row in topaslist)
            {
                int rowcount = Convert.ToInt32(row.BHNRCD);

                if (rowcount > 0)
                {

                    if (row.BHBHNO.Trim() != "")
                    {
                        sqlCmd = new iDB2Command();
                        sqlCmd.CommandText = "SELECT * FROM " + TopasSchema + ".P1BCEX1P WHERE EXBHNO = @BSBHNO AND EXSTS =''";
                        sqlCmd.Parameters.AddWithValue("@BSBHNO", row.BHBHNO.Trim());
                        DataTable P1BCEX1Pdata = Db2GetDataTable(sqlCmd, Db2TopasConnection());
                        List<P1BCEX1P> P1BCEX1Plist = new List<P1BCEX1P>();
                        P1BCEX1Plist = ConvertDataTable<P1BCEX1P>(P1BCEX1Pdata);

                        if (P1BCEX1Pdata.Rows.Count == rowcount)
                        {
                            int countrecords = 0;

                            foreach (var i in P1BCEX1Plist)
                            {
                                string orderNumber ="EX_" + i.EXBHNO + "_" + i.EXBPCD.Trim() + "_" + i.EXODTP.Trim() + "_" + i.EXTAXE.Trim();
                                if (true)
                                {
                                    string customerCode = i.EXBPCD.Trim() + "_" + i.EXODTP.Trim() + "_" + i.EXTAXE.Trim();

                                    string OrderType = i.EXODTP.Trim() == "2" ? "SEA" : "AIR";
                                    OrderType = "EX_" + OrderType + "_" + (i.EXBPCD.Trim() == "9722D88" ? "A" : "N") + i.EXTAXE.Trim();
                                    sqlCmd = new iDB2Command();
                                    sqlCmd.CommandText = "INSERT INTO " + GudangSysSchema + ".SalesOrderHeaders " +
                                        "(TenantCode,OrderNumber,WarehouseCode,OrderTypeCode,CustomerCode,CustomerName,CustomerOrderNumber," +
                                        "ShippingAddress,BillToShippingAddress,BillingAddress,DeliveryDate,Remarks,StatusCode,RevisionNumber," +
                                        "CreatedBy,CreatedOn,UpdatedBy,UpdatedOn,IsArchived,GRRef,AllowPartialPick,Reason,isAllocate,AllocationType," +
                                         "TallyClerkFee,OvertimeHours,OvertimeManpower,DeliveryType,DeliveryCharges,Route ) VALUES (" +
                                        "@TenantCode,@OrderNumber,@WarehouseCode,@OrderTypeCode,@CustomerCode,@CustomerName,@CustomerOrderNumber," +
                                        "@ShippingAddress,@BillToShippingAddress,@BillingAddress,@DeliveryDate,@Remarks,@StatusCode,@RevisionNumber," +
                                        "@CreatedBy,@CreatedOn,@UpdatedBy,@UpdatedOn,@IsArchived,@GRRef,@AllowPartialPick,@Reason,@isAllocate,@AllocationType," +
                                        "@TallyClerkFee,@OvertimeHours,@OvertimeManpower,@DeliveryType,@DeliveryCharges,'EXPORTS" + "_" + i.EXBPCD.Trim() + "_" + i.EXODTP.Trim() + "_" + i.EXTAXE.Trim() + "')";
                                    sqlCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
                                    sqlCmd.Parameters.AddWithValue("@OrderNumber".ToUpper(), orderNumber);
                                    sqlCmd.Parameters.AddWithValue("@WarehouseCode".ToUpper(), WarehouseCode);
                                    sqlCmd.Parameters.AddWithValue("@OrderTypeCode".ToUpper(), OrderType);
                                    sqlCmd.Parameters.AddWithValue("@CustomerCode".ToUpper(), customerCode);
                                    sqlCmd.Parameters.AddWithValue("@CustomerName".ToUpper(), i.EXBPCD.Trim());
                                    sqlCmd.Parameters.AddWithValue("@CustomerOrderNumber".ToUpper(), i.EXORDN.Trim());
                                    sqlCmd.Parameters.AddWithValue("@ShippingAddress".ToUpper(), "EXPORTS");
                                    sqlCmd.Parameters.AddWithValue("@BillToShippingAddress".ToUpper(), "1");
                                    sqlCmd.Parameters.AddWithValue("@BillingAddress".ToUpper(), "EXPORTS");
                                    sqlCmd.Parameters.AddWithValue("@DeliveryDate".ToUpper(), DateTime.ParseExact(i.EXPDAT.Trim(), "yyyyMMdd", null).ToString("yyyy-MM-dd"));
                                    sqlCmd.Parameters.AddWithValue("@Remarks".ToUpper(), i.EXRMRK.Trim());
                                    sqlCmd.Parameters.AddWithValue("@StatusCode".ToUpper(), "N");
                                    sqlCmd.Parameters.AddWithValue("@RevisionNumber".ToUpper(), "1");
                                    sqlCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
                                    sqlCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
                                    sqlCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
                                    sqlCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
                                    sqlCmd.Parameters.AddWithValue("@IsArchived".ToUpper(), "0");
                                    sqlCmd.Parameters.AddWithValue("@GRRef".ToUpper(), "");
                                    sqlCmd.Parameters.AddWithValue("@AllowPartialPick".ToUpper(), "1");
                                    sqlCmd.Parameters.AddWithValue("@Reason".ToUpper(), "");
                                    sqlCmd.Parameters.AddWithValue("@isAllocate".ToUpper(), "1");
                                    sqlCmd.Parameters.AddWithValue("@AllocationType".ToUpper(), DefaultAllocation);
                                    sqlCmd.Parameters.AddWithValue("@TallyClerkFee".ToUpper(), "0");
                                    sqlCmd.Parameters.AddWithValue("@OvertimeHours".ToUpper(), "0");
                                    sqlCmd.Parameters.AddWithValue("@OvertimeManpower".ToUpper(), "0");
                                    sqlCmd.Parameters.AddWithValue("@DeliveryType".ToUpper(), "EXPORTS");
                                    sqlCmd.Parameters.AddWithValue("@DeliveryCharges".ToUpper(), "0");

                                    Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                                }

                                iDB2Command sqlCmd2 = new iDB2Command();
                                sqlCmd2.CommandText = "INSERT INTO " + GudangSysSchema + ".SalesOrderDetails " +
                                    "( TenantCode,OrderNumber,ItemCode,ItemUomCode,UomLooseQty,ItemPrice,BatchNumber,LotNumber,ExpiryDate,RevisionNumber," +
                                    "OrderGoQty,AllocateOkQty,AllocateBoQty,PickGoQty,PickOkQty,PickLeQty,PickQuQty,CheckOkQty,CheckLeQty,CheckQuQty," +
                                    "PackOkQty,PackLeQty,PackQuQty,LoadOkQty,ShipOkQty,PendingShipQty,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn,IsArchived," +
                                    "Remark,IsPickItem,NewFlag,AlertFlag,GRBinnedLooseQty,LocationCode,AllocationType,CrossDockFlag,LineNumber," +
                                    "PalletBatch,PalletNumber,Key1,Key2) VALUES " +
                                    "(@TenantCode,@OrderNumber,@ItemCode,@ItemUomCode,@UomLooseQty,@ItemPrice,@BatchNumber,@LotNumber,@ExpiryDate,@RevisionNumber," +
                                    "@OrderGoQty,@AllocateOkQty,@AllocateBoQty,@PickGoQty,@PickOkQty,@PickLeQty,@PickQuQty,@CheckOkQty,@CheckLeQty,@CheckQuQty," +
                                    "@PackOkQty,@PackLeQty,@PackQuQty,@LoadOkQty,@ShipOkQty,@PendingShipQty,@CreatedBy,@CreatedOn,@UpdatedBy,@UpdatedOn,@IsArchived," +
                                    "@Remark,@IsPickItem,@NewFlag,@AlertFlag,@GRBinnedLooseQty,@LocationCode,@AllocationType,@CrossDockFlag,@LineNumber," +
                                    "   @PalletBatch,@PalletNumber,@Key1,@Key2)";
                                sqlCmd2.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
                                sqlCmd2.Parameters.AddWithValue("@OrderNumber".ToUpper(), orderNumber);
                                sqlCmd2.Parameters.AddWithValue("@ItemCode".ToUpper(), i.EXPNO.Trim());
                                sqlCmd2.Parameters.AddWithValue("@ItemUomCode".ToUpper(), "PCS");
                                sqlCmd2.Parameters.AddWithValue("@UomLooseQty".ToUpper(), "1");
                                sqlCmd2.Parameters.AddWithValue("@ItemPrice".ToUpper(), "0.00");
                                sqlCmd2.Parameters.AddWithValue("@BatchNumber".ToUpper(), "");
                                sqlCmd2.Parameters.AddWithValue("@LotNumber".ToUpper(), i.EXFRAN.Trim());
                                sqlCmd2.Parameters.AddWithValue("@ExpiryDate".ToUpper(), DBNull.Value);
                                sqlCmd2.Parameters.AddWithValue("@RevisionNumber".ToUpper(), "1");
                                sqlCmd2.Parameters.AddWithValue("@OrderGoQty".ToUpper(), i.EXIQTY.Trim());
                                sqlCmd2.Parameters.AddWithValue("@AllocateOkQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@AllocateBoQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@PickGoQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@PickOkQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@PickLeQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@PickQuQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@CheckOkQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@CheckLeQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@CheckQuQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@PackOkQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@PackLeQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@PackQuQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@LoadOkQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@ShipOkQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@PendingShipQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@CreatedBy".ToUpper(), "System");
                                sqlCmd2.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
                                sqlCmd2.Parameters.AddWithValue("@UpdatedBy".ToUpper(), "System");
                                sqlCmd2.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
                                sqlCmd2.Parameters.AddWithValue("@IsArchived".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@Remark".ToUpper(), "");
                                sqlCmd2.Parameters.AddWithValue("@IsPickItem".ToUpper(), "1");
                                sqlCmd2.Parameters.AddWithValue("@NewFlag".ToUpper(), "1");
                                sqlCmd2.Parameters.AddWithValue("@AlertFlag".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@GRBinnedLooseQty".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@LocationCode".ToUpper(), "");
                                sqlCmd2.Parameters.AddWithValue("@AllocationType".ToUpper(), DefaultAllocation);
                                sqlCmd2.Parameters.AddWithValue("@CrossDockFlag".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@LineNumber".ToUpper(), "0");
                                sqlCmd2.Parameters.AddWithValue("@PalletBatch".ToUpper(), DBNull.Value);
                                sqlCmd2.Parameters.AddWithValue("@PalletNumber".ToUpper(), DBNull.Value);
                                sqlCmd2.Parameters.AddWithValue("@Key1".ToUpper(), i.EXBHNO.Trim());
                                sqlCmd2.Parameters.AddWithValue("@Key2".ToUpper(), i.EXBITM.Trim());

                                Db2GetDataTable(sqlCmd2, Db2GudangSysConnection());
                                countrecords++;
                            }
                            flag = "X";
                        }
                        else
                        {
                            flag = "E";
                        }
                        iDB2Command updateCmd = new iDB2Command();
                        updateCmd.CommandText = "UPDATE " + TopasSchema + ".P1BCEXHP  SET " +
                            "BHFLAG = @BHFLAG,  BHUPDT= @BHUPDT, BHUPTM=@BHUPTM " +
                            "WHERE BHBHNO = @BHBHNO";
                        updateCmd.Parameters.AddWithValue("@BHBHNO", row.BHBHNO.Trim());
                        updateCmd.Parameters.AddWithValue("@BHUPDT", TopasDate(DateTime.Now));
                        updateCmd.Parameters.AddWithValue("@BHUPTM", TopasTime(DateTime.Now));
                        updateCmd.Parameters.AddWithValue("@BHFLAG", flag);
                        Db2GetDataTable(updateCmd, Db2TopasConnection());
                    }

                }

            }
            //end
        }


        private static void iP1BCPHP(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {

            string flag = "X";
            var userid = "";
            var DefaultAllocation = ConfigurationManager.AppSettings["DefaultAllocation"].ToString();

            iDB2Command sqlCmd = new iDB2Command();
            sqlCmd.CommandText = TopasSQL;
            DataTable topasdata = Db2GetDataTable(sqlCmd, Db2TopasConnection());

            sqlCmd = new iDB2Command();
            sqlCmd.CommandText = GSysSQL;
            DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

            List<P1BCPHP> topaslist = new List<P1BCPHP>();
            topaslist = ConvertDataTable<P1BCPHP>(topasdata);

            List<SalesOrderHeaders> gudangsyslist = new List<SalesOrderHeaders>();
            gudangsyslist = ConvertDataTable<SalesOrderHeaders>(gudangsysdata);


            foreach (P1BCPHP row in topaslist)
            {
                userid = row.BHUSER;
                int rowcount = Convert.ToInt32(row.BHNRCD);

                if (rowcount > 0)
                {
                    if (row.BHBHNO.Trim() != "")
                    {
                        sqlCmd = new iDB2Command();
                        sqlCmd.CommandText = "SELECT * FROM " + TopasSchema + ".P1BCPIP WHERE BPBHNO = @BHBHNO";
                        sqlCmd.Parameters.AddWithValue("@BHBHNO", row.BHBHNO.Trim());
                        DataTable P1BCPIPdata = Db2GetDataTable(sqlCmd, Db2TopasConnection());
                        List<P1BCPIP> P1BCPIPlist = new List<P1BCPIP>();
                        P1BCPIPlist = ConvertDataTable<P1BCPIP>(P1BCPIPdata);

                        if (P1BCPIPdata.Rows.Count == rowcount)
                        {
                            sqlCmd = new iDB2Command();
                            sqlCmd.CommandText = "SELECT * FROM " + GudangSysSchema + ".DeliveryAddress " +
                                "WHERE TenantCode = @TenantCode AND AddressName = 'Main'";
                            sqlCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
                            DataTable Addressdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                            List<DeliveryAddress> Addresslist = new List<DeliveryAddress>();
                            Addresslist = ConvertDataTable<DeliveryAddress>(Addressdata);

                            sqlCmd = new iDB2Command();
                            sqlCmd.CommandText = "SELECT * FROM " + GudangSysSchema + ".CustomerMaster " +
                                "WHERE TenantCode = @TenantCode ";
                            sqlCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);

                            DataTable Customerdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                            List<CustomerMaster> Customerlist = new List<CustomerMaster>();
                            Customerlist = ConvertDataTable<CustomerMaster>(Customerdata);

                            foreach (var order in P1BCPIPlist.Select(c => new { BPBHNO = c.BPBHNO.Trim(), BPCUNO = c.BPCUNO.Trim() }).Distinct())
                            {
                                //string tempCustCode = "";
                                //var custcode = P1BCPIPlist.Where(c => c.BPCUNO.Trim() == order.BPCUNO).Take(1);
                                //foreach (var maincustcode in custcode) { tempCustCode = maincustcode.BPCUNO.Trim(); }
                                string Address = "";
                                foreach (var add in Addresslist.Where(c => c.CustomerCode == order.BPCUNO).Take(1)) { Address = add.Address; }

                                string CustomerName = "";
                                foreach (var cust in Customerlist.Where(c => c.CustomerCode == order.BPCUNO).Take(1)) { CustomerName = cust.CustomerName; }

                                int countrecords = 0;
                                foreach (var i in P1BCPIPlist.Where(c => c.BPCUNO.Trim() == order.BPCUNO && c.BPCUNO.Trim() == order.BPCUNO.Trim()))
                                {
                                    string OrderNumber = i.BPBHNO.Trim() + "_" + i.BPCUNO.Trim();
                                    if (countrecords == 0)
                                    {
                                        string region = "";
                                        string routename = i.BPROUT.Trim();
                                       
                                        string OrderType = i.BPBHNO.Trim().Substring(0, 1) == "B" ? "DOMESTIC" : "";
                                        OrderType = i.BPBHNO.Trim().Substring(0, 1) == "E" ? "EM" : OrderType;
                                        if (routename == "RTI")
                                        {
                                            OrderType = "RTI";
                                            routename = "RTI ";
                                            //+ i.BPBHNO.Trim() + " " + i.BPCUNO.Trim();

                                        }

                                        sqlCmd = new iDB2Command();
                                        sqlCmd.CommandText = "INSERT INTO " + GudangSysSchema + ".SalesOrderHeaders " +
                                            "(TenantCode,OrderNumber,WarehouseCode,OrderTypeCode,CustomerCode,CustomerName,CustomerOrderNumber," +
                                            "ShippingAddress,BillToShippingAddress,BillingAddress,DeliveryDate,Remarks,StatusCode,RevisionNumber," +
                                            "CreatedBy,CreatedOn,UpdatedBy,UpdatedOn,IsArchived,GRRef,AllowPartialPick,Reason,isAllocate,AllocationType," +
                                            "TallyClerkFee,OvertimeHours,OvertimeManpower,DeliveryType,DeliveryCharges,Route,Region ) VALUES (" +
                                            "@TenantCode,@OrderNumber,@WarehouseCode,@OrderTypeCode,@CustomerCode,@CustomerName,@CustomerOrderNumber," +
                                            "@ShippingAddress,@BillToShippingAddress,@BillingAddress,@DeliveryDate,@Remarks,@StatusCode,@RevisionNumber," +
                                            "@CreatedBy,@CreatedOn,@UpdatedBy,@UpdatedOn,@IsArchived,@GRRef,@AllowPartialPick,@Reason,@isAllocate,@AllocationType," +
                                            "@TallyClerkFee,@OvertimeHours,@OvertimeManpower,@DeliveryType,@DeliveryCharges,'" + routename + "','" + region + "')";
                                        sqlCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
                                        sqlCmd.Parameters.AddWithValue("@OrderNumber".ToUpper(), OrderNumber);
                                        sqlCmd.Parameters.AddWithValue("@WarehouseCode".ToUpper(), WarehouseCode);
                                        sqlCmd.Parameters.AddWithValue("@OrderTypeCode".ToUpper(), OrderType);
                                        sqlCmd.Parameters.AddWithValue("@CustomerCode".ToUpper(), i.BPCUNO.Trim());
                                        sqlCmd.Parameters.AddWithValue("@CustomerName".ToUpper(), CustomerName);
                                        sqlCmd.Parameters.AddWithValue("@CustomerOrderNumber".ToUpper(), i.BPSINV.Trim() != "" ? i.BPSINV.Trim() : i.BPROUT.Trim());
                                        sqlCmd.Parameters.AddWithValue("@ShippingAddress".ToUpper(), Address);
                                        sqlCmd.Parameters.AddWithValue("@BillToShippingAddress".ToUpper(), "1");
                                        sqlCmd.Parameters.AddWithValue("@BillingAddress".ToUpper(), Address);
                                        sqlCmd.Parameters.AddWithValue("@DeliveryDate".ToUpper(), DateTime.ParseExact(i.BPPDAT.Trim(), "yyyyMMdd", null).ToString("yyyy-MM-dd"));
                                        sqlCmd.Parameters.AddWithValue("@Remarks".ToUpper(), i.BPREMK.Trim());
                                        sqlCmd.Parameters.AddWithValue("@StatusCode".ToUpper(), "A");
                                        sqlCmd.Parameters.AddWithValue("@RevisionNumber".ToUpper(), "1");
                                        sqlCmd.Parameters.AddWithValue("@CreatedBy".ToUpper(), userid);
                                        sqlCmd.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
                                        sqlCmd.Parameters.AddWithValue("@UpdatedBy".ToUpper(), userid);
                                        sqlCmd.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
                                        sqlCmd.Parameters.AddWithValue("@IsArchived".ToUpper(), "0");
                                        sqlCmd.Parameters.AddWithValue("@GRRef".ToUpper(), "");
                                        sqlCmd.Parameters.AddWithValue("@AllowPartialPick".ToUpper(), "1");
                                        sqlCmd.Parameters.AddWithValue("@Reason".ToUpper(), "");
                                        sqlCmd.Parameters.AddWithValue("@isAllocate".ToUpper(), "1");
                                        sqlCmd.Parameters.AddWithValue("@AllocationType".ToUpper(), DefaultAllocation);
                                        sqlCmd.Parameters.AddWithValue("@TallyClerkFee".ToUpper(), "0");
                                        sqlCmd.Parameters.AddWithValue("@OvertimeHours".ToUpper(), "0");
                                        sqlCmd.Parameters.AddWithValue("@OvertimeManpower".ToUpper(), "0");
                                        sqlCmd.Parameters.AddWithValue("@DeliveryType".ToUpper(), OrderType);
                                        sqlCmd.Parameters.AddWithValue("@DeliveryCharges".ToUpper(), "0");

                                        Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                                    }

                                    iDB2Command sqlCmd2 = new iDB2Command();
                                    sqlCmd2.CommandText = "INSERT INTO " + GudangSysSchema + ".SalesOrderDetails " +
                                        "(TenantCode,OrderNumber,ItemCode,ItemUomCode,UomLooseQty,ItemPrice,BatchNumber,LotNumber,ExpiryDate,RevisionNumber," +
                                        "OrderGoQty,AllocateOkQty,AllocateBoQty,PickGoQty,PickOkQty,PickLeQty,PickQuQty,CheckOkQty,CheckLeQty,CheckQuQty," +
                                        "PackOkQty,PackLeQty,PackQuQty,LoadOkQty,ShipOkQty,PendingShipQty,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn,IsArchived," +
                                        "Remark,IsPickItem,NewFlag,AlertFlag,GRBinnedLooseQty,LocationCode,AllocationType,CrossDockFlag,LineNumber," +
                                        "PalletBatch,PalletNumber,Key1,Key2,CustomField4) VALUES " +
                                        "(@TenantCode,@OrderNumber,@ItemCode,@ItemUomCode,@UomLooseQty,@ItemPrice,@BatchNumber,@LotNumber,@ExpiryDate,@RevisionNumber," +
                                        "@OrderGoQty,@AllocateOkQty,@AllocateBoQty,@PickGoQty,@PickOkQty,@PickLeQty,@PickQuQty,@CheckOkQty,@CheckLeQty,@CheckQuQty," +
                                        "@PackOkQty,@PackLeQty,@PackQuQty,@LoadOkQty,@ShipOkQty,@PendingShipQty,@CreatedBy,@CreatedOn,@UpdatedBy,@UpdatedOn,@IsArchived," +
                                        "@Remark,@IsPickItem,@NewFlag,@AlertFlag,@GRBinnedLooseQty,@LocationCode,@AllocationType,@CrossDockFlag,@LineNumber," +
                                        "   @PalletBatch,@PalletNumber,@Key1,@Key2,'" + i.BPODTP + "')";
                                    sqlCmd2.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
                                    sqlCmd2.Parameters.AddWithValue("@OrderNumber".ToUpper(), OrderNumber);
                                    sqlCmd2.Parameters.AddWithValue("@ItemCode".ToUpper(), i.BPPNO.Trim());
                                    sqlCmd2.Parameters.AddWithValue("@ItemUomCode".ToUpper(), "PCS");
                                    sqlCmd2.Parameters.AddWithValue("@UomLooseQty".ToUpper(), "1");
                                    sqlCmd2.Parameters.AddWithValue("@ItemPrice".ToUpper(), "0.00");
                                    sqlCmd2.Parameters.AddWithValue("@BatchNumber".ToUpper(), i.BPCASE.Trim());
                                    sqlCmd2.Parameters.AddWithValue("@LotNumber".ToUpper(), i.BPFRAN.Trim());
                                    sqlCmd2.Parameters.AddWithValue("@ExpiryDate".ToUpper(), DBNull.Value);
                                    sqlCmd2.Parameters.AddWithValue("@RevisionNumber".ToUpper(), "1");
                                    sqlCmd2.Parameters.AddWithValue("@OrderGoQty".ToUpper(), i.BPAQTY.Trim());
                                    sqlCmd2.Parameters.AddWithValue("@AllocateOkQty".ToUpper(), i.BPROUT.Trim().StartsWith("P2") ? i.BPAQTY.Trim() : "0");
                                    sqlCmd2.Parameters.AddWithValue("@AllocateBoQty".ToUpper(), "0");
                                    sqlCmd2.Parameters.AddWithValue("@PickGoQty".ToUpper(), i.BPROUT.Trim().StartsWith("P2") ? i.BPAQTY.Trim() : "0");
                                    sqlCmd2.Parameters.AddWithValue("@PickOkQty".ToUpper(), i.BPROUT.Trim().StartsWith("P2") ? i.BPAQTY.Trim() : "0");
                                    sqlCmd2.Parameters.AddWithValue("@PickLeQty".ToUpper(), "0");
                                    sqlCmd2.Parameters.AddWithValue("@PickQuQty".ToUpper(), "0");
                                    sqlCmd2.Parameters.AddWithValue("@CheckOkQty".ToUpper(), i.BPROUT.Trim().StartsWith("P2") ? i.BPAQTY.Trim() : "0");
                                    sqlCmd2.Parameters.AddWithValue("@CheckLeQty".ToUpper(), "0");
                                    sqlCmd2.Parameters.AddWithValue("@CheckQuQty".ToUpper(), "0");
                                    sqlCmd2.Parameters.AddWithValue("@PackOkQty".ToUpper(), i.BPROUT.Trim().StartsWith("P2") ? i.BPAQTY.Trim() : "0");
                                    sqlCmd2.Parameters.AddWithValue("@PackLeQty".ToUpper(), "0");
                                    sqlCmd2.Parameters.AddWithValue("@PackQuQty".ToUpper(), "0");
                                    sqlCmd2.Parameters.AddWithValue("@LoadOkQty".ToUpper(), "0");
                                    sqlCmd2.Parameters.AddWithValue("@ShipOkQty".ToUpper(), "0");
                                    sqlCmd2.Parameters.AddWithValue("@PendingShipQty".ToUpper(), "0");
                                    sqlCmd2.Parameters.AddWithValue("@CreatedBy".ToUpper(), userid);
                                    sqlCmd2.Parameters.AddWithValue("@CreatedOn".ToUpper(), datestamp);
                                    sqlCmd2.Parameters.AddWithValue("@UpdatedBy".ToUpper(), userid);
                                    sqlCmd2.Parameters.AddWithValue("@UpdatedOn".ToUpper(), datestamp);
                                    sqlCmd2.Parameters.AddWithValue("@IsArchived".ToUpper(), "0");
                                    sqlCmd2.Parameters.AddWithValue("@Remark".ToUpper(), i.BPORDN.Trim());
                                    sqlCmd2.Parameters.AddWithValue("@IsPickItem".ToUpper(), "1");
                                    sqlCmd2.Parameters.AddWithValue("@NewFlag".ToUpper(), "1");
                                    sqlCmd2.Parameters.AddWithValue("@AlertFlag".ToUpper(), "0");
                                    sqlCmd2.Parameters.AddWithValue("@GRBinnedLooseQty".ToUpper(), "0");
                                    sqlCmd2.Parameters.AddWithValue("@LocationCode".ToUpper(), "");
                                    sqlCmd2.Parameters.AddWithValue("@AllocationType".ToUpper(), DefaultAllocation);
                                    sqlCmd2.Parameters.AddWithValue("@CrossDockFlag".ToUpper(), i.BPSINV.Trim() != "" && i.BPROUT.Trim().StartsWith("P2") ? "1" : "0");
                                    sqlCmd2.Parameters.AddWithValue("@LineNumber".ToUpper(), "0");
                                    sqlCmd2.Parameters.AddWithValue("@PalletBatch".ToUpper(), DBNull.Value);
                                    sqlCmd2.Parameters.AddWithValue("@PalletNumber".ToUpper(), DBNull.Value);
                                    sqlCmd2.Parameters.AddWithValue("@Key1".ToUpper(), i.BPBHNO.Trim());
                                    sqlCmd2.Parameters.AddWithValue("@Key2".ToUpper(), i.BPBITM.Trim());

                                    Db2GetDataTable(sqlCmd2, Db2GudangSysConnection());
                                    countrecords++;
                                }
                            }
                            iDB2Command updateCmd2 = new iDB2Command();

                            flag = "X";

                            updateCmd2.CommandText = "MERGE INTO " + GudangSysSchema + ".SalesOrderHeaders destination " +
                                "Using (select DISTINCT TenantCode,CustomerCode,address  from " + GudangSysSchema + ".DeliveryAddress where primarydeliveryaddress = 1) origin " +
                                "ON (origin.TenantCode  = destination.TenantCode AND origin.CustomerCode  = destination.CustomerCode )   " +
                                "WHEN MATCHED AND destination.shippingaddress = ''  THEN UPDATE SET  shippingaddress = origin.address";

                            Db2GetDataTable(updateCmd2, Db2GudangSysConnection());

                            updateCmd2 = new iDB2Command();
                            updateCmd2.CommandText = "MERGE INTO " + GudangSysSchema + ".SalesOrderHeaders destination " +
                               "Using (select DISTINCT TenantCode,CustomerCode,CustomerName  from " + GudangSysSchema + ".CustomerMaster ) origin " +
                               "ON (origin.TenantCode  = destination.TenantCode AND origin.CustomerCode  = destination.CustomerCode )   " +
                               "WHEN MATCHED AND destination.CustomerName = ''  THEN UPDATE SET  CustomerName = origin.CustomerName";
                            Db2GetDataTable(updateCmd2, Db2GudangSysConnection());

                            updateCmd2 = new iDB2Command();
                            updateCmd2.CommandText = "MERGE INTO " + GudangSysSchema + ".SalesOrderHeaders destination " +
                               "Using (SELECT Route, Description FROM " + GudangSysSchema + ".DeliveryChargesRoute) origin " +
                               "ON (origin.Route  = destination.Route )   " +
                               "WHEN MATCHED AND  destination.Region is null   THEN UPDATE SET  Region = origin.Description";
                            Db2GetDataTable(updateCmd2, Db2GudangSysConnection());

                            foreach (var i in P1BCPIPlist.Where(b => b.BPCASE != "").Select(b => new { BPCASE = b.BPCASE }).Distinct().ToList())
                            {
                                updateCmd2 = new iDB2Command();
                                updateCmd2.CommandText = "UPDATE " + GudangSysSchema + ".CrossDockPacking  " +
                                   "SET Status = 'Ready' WHERE CaseNumber ='" + i.BPCASE + "' ";
                                Db2GetDataTable(updateCmd2, Db2GudangSysConnection());

                                updateCmd2 = new iDB2Command();
                                updateCmd2.CommandText = "UPDATE " + GudangSysSchema + ".CaseLabelRef  " +
                                   "SET Status = 'Ready' WHERE CaseNumber ='" + i.BPCASE + "' ";
                                Db2GetDataTable(updateCmd2, Db2GudangSysConnection());
                            }


                        }
                        else
                        {
                            flag = "E";
                        }



                        iDB2Command updateCmd = new iDB2Command();
                        updateCmd.CommandText = "UPDATE " + TopasSchema + ".P1BCPHP  SET " +
                            "BHFLAG = @BHFLAG,  BHUPDT= @BHUPDT, BHUPTM=@BHUPTM " +
                            "WHERE BHBHNO = @BHBHNO";
                        updateCmd.Parameters.AddWithValue("@BHBHNO", row.BHBHNO.Trim());
                        updateCmd.Parameters.AddWithValue("@BHUPDT", TopasDate(DateTime.Now));
                        updateCmd.Parameters.AddWithValue("@BHUPTM", TopasTime(DateTime.Now));
                        updateCmd.Parameters.AddWithValue("@BHFLAG", flag);

                        updateCmd.CommandText = updateCmd.CommandText.ToUpper();
                        Db2GetDataTable(updateCmd, Db2TopasConnection());
                    }

                }

            }
            //end
        }

        private static void iP1STOC1P(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {
            iDB2Command updateCmd = new iDB2Command();
            updateCmd.CommandText = "DELETE " + GudangSysSchema + ".ERPDailyInventory_Snapshot " +
                           "where DateStamp = @DateStamp AND WarehouseCode= @WarehouseCode ";
            updateCmd.Parameters.AddWithValue("@DateStamp".ToUpper(), datestamp.ToString("yyyy-MM-dd"));
            updateCmd.Parameters.AddWithValue("@WarehouseCode".ToUpper(), WarehouseCode);
            Db2GetDataTable(updateCmd, Db2GudangSysConnection());

            updateCmd = new iDB2Command();
            updateCmd.CommandText = "INSERT INTO " + GudangSysSchema + ".ERPDailyInventory_Snapshot " +
                "(DateStamp,ClientAccount,ItemNo,BatchNumber,LotNumber,Qty,StockCountId,WarehouseCode,LocationCode)  " +
                "SELECT '" + datestamp.ToString("yyyy-MM-dd") + "','" + ClientAccount + "',trim(STPNO) as STPNO,'','', trim(STOHB) as STOHB,'0','" + WarehouseCode + "'," +
                "trim(STLOCB) as  STLOCB FROM " + TopasSchema + ".P1STOC1P";

            Db2GetDataTable(updateCmd, Db2GudangSysConnection());

            //iDB2Command sqlCmd = new iDB2Command();
            //sqlCmd.CommandText = TopasSQL;
            //DataTable topasdata = Db2GetDataTable(sqlCmd, Db2TopasConnection());

            //sqlCmd = new iDB2Command();
            //sqlCmd.CommandText = GSysSQL;
            //DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

            //List<P1STOC1P> topaslist = new List<P1STOC1P>();
            //topaslist = ConvertDataTable<P1STOC1P>(topasdata);

            //List<ERPDailyInventory_Snapshot> gudangsyslist = new List<ERPDailyInventory_Snapshot>();
            //gudangsyslist = ConvertDataTable<ERPDailyInventory_Snapshot>(gudangsysdata);

            //if (topaslist.Count > 0)
            //{
            //    foreach (P1STOC1P row in topaslist)
            //    {

            //        if (gudangsyslist.Where(c => c.DateStamp == datestamp.Date.ToShortDateString()).Count() > 0)
            //        {


            //        }

            //        updateCmd = new iDB2Command();
            //        updateCmd.CommandType = CommandType.Text;
            //        updateCmd.CommandText = "INSERT INTO " + GudangSysSchema + ".\"ERPDailyInventory_Snapshot\" " +
            //            "(\"DateStamp\",\"ClientAccount\",\"ItemNo\",\"BatchNumber\",\"LotNumber\",\"Qty\",\"StockCountId\",\"WarehouseCode\",\"LocationCode\") VALUES " +
            //            "( @DateStamp,@ClientAccount,@ItemNo,@BatchNumber,@LotNumber,@Qty,@StockCountId,@WarehouseCode,@LocationCode)";
            //        updateCmd.Parameters.AddWithValue("DateStamp".ToUpper(), datestamp.ToString("yyyy-MM-dd"));
            //        updateCmd.Parameters.AddWithValue("ClientAccount".ToUpper(), ClientAccount);
            //        updateCmd.Parameters.AddWithValue("ItemNo".ToUpper(), row.STPNO.Trim());
            //        updateCmd.Parameters.AddWithValue("BatchNumber".ToUpper(), "");
            //        updateCmd.Parameters.AddWithValue("LotNumber".ToUpper(), row.STFRAN.Trim());
            //        updateCmd.Parameters.AddWithValue("Qty".ToUpper(), row.STOHB.Trim());
            //        updateCmd.Parameters.AddWithValue("StockCountId".ToUpper(), 0);
            //        updateCmd.Parameters.AddWithValue("WarehouseCode".ToUpper(), WarehouseCode);
            //        updateCmd.Parameters.AddWithValue("LocationCode".ToUpper(), row.STLOCB.Trim());

            //        Db2GetDataTable(updateCmd, Db2GudangSysConnection());

            //    }
            //}

            //end
        }

        //Receiving Submission
        private static void iP1BP2PHP(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {
            string TopasDateString = datestamp.ToString("yyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            string TopasTimeString = DateTime.Now.ToString("HHmm", System.Globalization.CultureInfo.InvariantCulture);
            string JobId = "";
            int BinCount = 0;
            int DenialCount = 0;
            int OutstandingCount = 0;
            int counter = 1;
            bool processFlag = false;

            iDB2Command sqlCmd = new iDB2Command();
            sqlCmd.CommandText = "SELECT distinct  trim(a.TenantCode) as TenantCode,trim(a.OrderNumber) as OrderNumber," +
                "a.TypeCode as TypeCode, trim(a.StatusCode) as StatusCode ,trim(a.WarehouseCode) as WarehouseCode, b.Remark as Remark, " +
                "case when b.PriorityFlag = 1 then b.CaseNumber else '' end as CaseNumber " +
                "FROM " + GudangSysSchema + ".RcvOrderHeaders a JOIN " + GudangSysSchema + ".RcvOrderDetails b " +
                "ON a.TenantCode = b.TenantCode AND a.OrderNumber = b.OrderNumber " +
                " WHERE  b.SendFlag = 1  AND b.customfield3 ='Submitted'";
            DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

            List<RcvOrderHeaders> gudangsyslist = new List<RcvOrderHeaders>();
            gudangsyslist = ConvertDataTable<RcvOrderHeaders>(gudangsysdata);

            foreach (RcvOrderHeaders row in gudangsyslist.OrderByDescending(c => c.CASENUMBER))
            {
                JobId = "";
                //reuse old job id
                if (row.REMARK != "" && row.REMARK.IndexOf("-BC-") > -1)
                {
                    JobId = row.REMARK.Trim();

                    // delete old submission record
                    sqlCmd = new iDB2Command();
                    sqlCmd.CommandText = "DELETE " + TopasSchema + ".P1BP2P2P  WHERE P2JBID = '" + JobId + "' ";
                    Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                    sqlCmd = new iDB2Command();
                    sqlCmd.CommandText = "DELETE " + TopasSchema + ".P1BP2P3P  WHERE P2JBID = '" + JobId + "' ";
                    Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                    sqlCmd = new iDB2Command();
                    sqlCmd.CommandText = "DELETE " + TopasSchema + ".P1BP2PHP  WHERE PHJBID = '" + JobId + "' ";
                    Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                }

                sqlCmd = new iDB2Command();

                sqlCmd.CommandText =
                          "WITH a as (select TenantCode,CaseNumber, RcvOrderDetailId,OrderNumber , SUM(PackOkqty) as PackOkqty " +
                              "from " + GudangSysSchema + ".CrossDockPacking where OrderNumber= '" + row.ORDERNUMBER.Trim() + "'  " +
                              "and CaseNumber <> '' group by TenantCode,CaseNumber, RcvOrderDetailId,OrderNumber ) " +
                              "SELECT trim(rod.StatusCode) as StatusCode, trim(rod.IsArchived) as IsArchived,  " +
                              "trim(rod.BinOkQty) as BinOkQty, trim(rod.BinQuQty) as  BinOkQty,trim(rod.BinLeQty) as BinLeQty, trim(rod.CheckLeQty) as CheckLeQty," +
                              "rod.PriorityFlag, rod.CaseNumber " +
                              "FROM " + GudangSysSchema + ".RcvOrderDetails  AS rod LEFT JOIN a AS cdp ON " +
                              "rod.TenantCode = cdp.TenantCode AND rod.OrderNumber = cdp.OrderNumber " +
                              "AND rod.RcvOrderDetailId = cdp.RcvOrderDetailId " +
                      "WHERE rod.TenantCode = '" + row.TENANTCODE.Trim() + "' AND rod.OrderNumber = '" + row.ORDERNUMBER.Trim() + "' " +
                      "AND rod.BinOkQty + rod.BinQuQty + rod.CheckLeQty >0 AND rod.CaseNumber LIKE '%" + row.CASENUMBER + "%' " +
                      "AND (rod.SendFlag = 1)";

                //sqlCmd.CommandText = 
                //    "SELECT trim(StatusCode) as StatusCode, trim(IsArchived) as IsArchived, trim(BinOkQty) as BinOkQty, " +
                //    "trim(BinQuQty) as  BinOkQty, " +
                //    "trim(BinLeQty) as BinLeQty, trim(CheckLeQty) as CheckLeQty,PriorityFlag, CaseNumber " +
                //    "FROM " + GudangSysSchema + ".RcvOrderDetails " +
                //    "WHERE trim(TenantCode) = @TenantCode AND trim(OrderNumber) = @OrderNumber AND IsArchived = 0 AND caseNumber like '%" + row.CASENUMBER + "%' " +
                //    "AND (trim(Remark) = @JobId OR Remark is null OR trim(Remark) = '') AND customfield3 ='Submitted'";
                //sqlCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), row.TENANTCODE.Trim());
                //sqlCmd.Parameters.AddWithValue("@OrderNumber".ToUpper(), row.ORDERNUMBER.Trim());
                //sqlCmd.Parameters.AddWithValue("@JobId".ToUpper(), JobId.Trim());
                DataTable ASNdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                List<RcvOrderDetails> ASNlist = new List<RcvOrderDetails>();
                ASNlist = ConvertDataTable<RcvOrderDetails>(ASNdata);

                OutstandingCount = ASNlist.Where(c => c.STATUSCODE == "A" || c.STATUSCODE == "N").Count();

                if (OutstandingCount == 0) { processFlag = true; } else { processFlag = false; };
                if (processFlag)
                {
                    BinCount = ASNlist.Where(c => (c.STATUSCODE == "C" && Convert.ToInt32(c.BINOKQTY) > 0)).Count();

                    DenialCount = ASNlist.Where(c => (Convert.ToInt32(c.BINLEQTY) > 0 || Convert.ToInt32(c.CHECKLEQTY) > 0) && c.STATUSCODE == "C").Count();
                    int countrecords = ASNlist.Count();
                    if (BinCount + DenialCount > 0 && OutstandingCount == 0)
                    {
                        //Single statement insert
                        processFlag = false;
                        if (JobId == "")
                            //JobId = TopasDateString + "-BC-" + TopasTimeString + "-" + counter.ToString();
                            JobId =  "BC-" + row.ORDERNUMBER.Trim() + (row.CASENUMBER =="" ? "" : "-" + row.CASENUMBER);
                        iDB2Command updateCmd = new iDB2Command();
                        //updateCmd.CommandText =
                        //     "INSERT INTO " + TopasSchema + ".P1BP2P2P " +
                        //        "(P2CRDT, P2JBID, P2INVN, P2BHNO, P2BITM, P2FRAN, P2PNO, P2PCAS, P2PQTY, P2FPNT,P2CASE) " +
                        //    "WITH a as (select TenantCode,CaseNumber, RcvOrderDetailId,OrderNumber , SUM(PackOkqty) as PackOkqty " +
                        //        "from " + GudangSysSchema + ".CrossDockPacking where OrderNumber= '" + row.ORDERNUMBER.Trim() + "'  " +
                        //        "and CaseNumber <> '' group by TenantCode,CaseNumber, RcvOrderDetailId,OrderNumber ) " +
                        //        "SELECT '" + TopasDate(datestamp) + "', '" + JobId + "', rod.OrderNumber,rod.Key1,rod.Key2, " +
                        //        "rod.LotNumber, rod.ItemCode,COALESCE(cdp.CaseNumber,''), " +
                        //        "case when rod.CrossDockFlag=0 or rod.CrossDockFlag is null  then rod.BinOkQty + rod.BinQuQty else cdp.PackOkQty End ,'',rod.CASENUMBER  " +
                        //        "FROM " + GudangSysSchema + ".RcvOrderDetails  AS rod LEFT JOIN a AS cdp ON " +
                        //        "rod.TenantCode = cdp.TenantCode AND rod.OrderNumber = cdp.OrderNumber " +
                        //        "AND rod.RcvOrderDetailId = cdp.RcvOrderDetailId " +
                        //"WHERE rod.TenantCode = '" + row.TENANTCODE.Trim() + "' AND rod.OrderNumber = '" + row.ORDERNUMBER.Trim() + "' " +
                        //"AND rod.BinOkQty + rod.BinQuQty >0 AND rod.CaseNumber LIKE '%" + row.CASENUMBER + "%' " +
                        //"AND (rod.SendFlag = 1) AND (rod.Remark = '" + JobId + "' OR rod.remark ='' OR rod.Remark is null )";
                        updateCmd.CommandText =
                             "INSERT INTO " + TopasSchema + ".P1BP2P2P " +
                                "(P2CRDT, P2JBID, P2INVN, P2BHNO, P2BITM, P2FRAN, P2PNO, P2PCAS, P2PQTY, P2FPNT,P2CASE) " +
                           "WITH a as (select TenantCode,CaseNumber, RcvOrderDetailId,OrderNumber , SUM(PackOkqty) as PackOkqty " +
                              "from " + GudangSysSchema + ".CrossDockPacking where OrderNumber= '" + row.ORDERNUMBER.Trim() + "'  " +
                              "and CaseNumber <> '' group by TenantCode,CaseNumber, RcvOrderDetailId,OrderNumber ) " +
                                "SELECT '" + TopasDate(datestamp) + "', '" + JobId + "', rod.OrderNumber,rod.Key1,rod.Key2, " +
                                "rod.LotNumber, rod.ItemCode,COALESCE(cdp.CaseNumber,''), " +
                                "case when rod.CrossDockFlag=0 or rod.CrossDockFlag is null  then rod.BinOkQty + rod.BinQuQty else cdp.PackOkQty End ,'',rod.CASENUMBER  " +
                                "FROM " + GudangSysSchema + ".RcvOrderDetails  AS rod LEFT JOIN a AS cdp ON " +
                              "rod.TenantCode = cdp.TenantCode AND rod.OrderNumber = cdp.OrderNumber " +
                              "AND rod.RcvOrderDetailId = cdp.RcvOrderDetailId " +
                      "WHERE rod.TenantCode = '" + row.TENANTCODE.Trim() + "' AND rod.OrderNumber = '" + row.ORDERNUMBER.Trim() + "' " +
                      "AND rod.BinOkQty + rod.BinQuQty >0 AND rod.CaseNumber LIKE '%" + row.CASENUMBER + "%' " +
                      "AND (rod.SendFlag = 1) ";

                       // Console.WriteLine(updateCmd.CommandText);
                        Db2GetDataTable(updateCmd, Db2TopasConnection());

                        updateCmd = new iDB2Command();
                        updateCmd.CommandText = "WITH a as (select TenantCode,CaseNumber, RcvOrderDetailId,OrderNumber , SUM(PackOkqty) as PackOkqty " +
                                "from " + GudangSysSchema + ".CrossDockPacking where OrderNumber= '" + row.ORDERNUMBER.Trim() + "'  " +
                                "and CaseNumber <> '' AND CaseNumber like '%" + row.CASENUMBER + "%' group by TenantCode,CaseNumber, RcvOrderDetailId,OrderNumber ) " +
                                "SELECT '" + TopasDate(datestamp) + "', '" + JobId + "', rod.OrderNumber,rod.Key1,rod.Key2, " +
                                "rod.LotNumber, rod.ItemCode,COALESCE(cdp.CaseNumber,''), case when cdp.CaseNumber is null then rod.BinOkQty + rod.BinQuQty else cdp.PackOkQty End ,''  " +
                                "FROM " + GudangSysSchema + ".RcvOrderDetails  AS rod LEFT JOIN a AS cdp ON rod.TenantCode = cdp.TenantCode " +
                                "AND rod.OrderNumber = cdp.OrderNumber AND rod.RcvOrderDetailId = cdp.RcvOrderDetailId " +
                        "WHERE rod.TenantCode = '" + row.TENANTCODE.Trim() + "' AND rod.OrderNumber = '" + row.ORDERNUMBER.Trim() + "' AND rod.BinOkQty + rod.BinQuQty >0 " +
                        "AND (rod.SendFlag = 1) AND (rod.Remark = '" + JobId + "' OR rod.Remark is null or rod.Remark = '') " +
                        "AND rod.CaseNumber like '%" + row.CASENUMBER + "%' ";
                        DataTable binlist = Db2GetDataTable(updateCmd, Db2TopasConnection());



                        updateCmd = new iDB2Command();
                        updateCmd.CommandText = "INSERT INTO " + TopasSchema + ".P1BP2P3P " +
                                "(P2DNDT, P2JBID, P2INVN, P2BHNO, P2BITM, P2FRAN, P2PNO, P2DNQT, P2RSCD, P2FPNT) " +
                                "SELECT '" + TopasDate(datestamp) + "', '" + JobId + "', a.OrderNumber,a.Key1,a.Key2, a.LotNumber, a.ItemCode, sum(b.Qty) as Qty,dr.Reason,''  " +
                                "  FROM " + GudangSysSchema + ".RcvOrderDetails a JOIN " + GudangSysSchema + ".rcvordertx b ON a.RcvOrderDetailId = b.RcvOrderDetailId  JOIN " + GudangSysSchema + ".DenialReason AS dr ON b.DenialReasonId = dr.id " +
                        "WHERE a.TenantCode = '" + row.TENANTCODE.Trim() + "' AND a.OrderNumber = '" + row.ORDERNUMBER.Trim() + "' AND (a.BinLeQty >0 OR a.CheckLeQty>0) " +
                        " AND (a.SendFlag = 1) AND (a.Remark = '" + JobId + "' OR a.Remark is null or a.Remark = '') " +
                        "AND b.RcvTxTypeCode LIKE '%LE' AND a.CaseNumber lIKE '%" + row.CASENUMBER + "%' " +
                        "GROUP BY a.OrderNumber,a.Key1,a.Key2, a.LotNumber, a.ItemCode,dr.Reason";
                        //Console.WriteLine(updateCmd.CommandText);
                        Db2GetDataTable(updateCmd, Db2TopasConnection());

                        updateCmd = new iDB2Command();
                        updateCmd.CommandText = "SELECT '" + TopasDate(datestamp) + "', '" + JobId + "', a.OrderNumber," +
                            "a.Key1,a.Key2, a.LotNumber, a.ItemCode, SUM(b.Qty) AS Qty,dr.Reason,''  " +
                                "  FROM " + GudangSysSchema + ".RcvOrderDetails a JOIN " + GudangSysSchema + ".rcvordertx b ON a.RcvOrderDetailId = b.RcvOrderDetailId  " +
                                "JOIN " + GudangSysSchema + ".DenialReason AS dr ON b.DenialReasonId = dr.id " +
                        "WHERE a.TenantCode = '" + row.TENANTCODE.Trim() + "' AND a.OrderNumber = '" + row.ORDERNUMBER.Trim() + "' AND (a.BinLeQty >0 OR a.CheckLeQty>0) " +
                        " AND (a.SendFlag = 1) AND (a.Remark = '" + JobId + "' OR a.Remark is null or a.Remark = '') AND b.RcvTxTypeCode LIKE '%LE' " +
                        "AND a.CaseNumber LIKE '%" + row.CASENUMBER + "%' AND a.OrderNumber= '" + row.ORDERNUMBER.Trim() + "' " +
                        " GROUP BY a.OrderNumber,a.Key1,a.Key2, a.LotNumber, a.ItemCode,dr.Reason";

                        DataTable deniallist = Db2GetDataTable(updateCmd, Db2TopasConnection());
                        DenialCount = deniallist.Rows.Count;

                        updateCmd = new iDB2Command();
                        updateCmd.CommandText = "INSERT INTO " + TopasSchema + ".P1BP2PHP " +
                        "(PHCRDT, PHJBID, PHINVN, PHCASE, PHNRC2, PHNRC3, PHUSER, PHUPDT, PHUPTM, PHSTAT, PHFPNT)  " +
                        "VALUES  ('" + TopasDate(datestamp) + "', '" + JobId + "', '" + row.ORDERNUMBER.Trim() + "','" + row.CASENUMBER + "'," +
                        "'" + BinCount.ToString() + "','" + DenialCount.ToString() + "','TBIPS','" + TopasDate(datestamp) + "', '" + TopasTime(datestamp) + "','','') ";
                        //Console.WriteLine(updateCmd.CommandText);
                        Db2GetDataTable(updateCmd, Db2TopasConnection());


                        updateCmd = new iDB2Command();
                        updateCmd.CommandText = "UPDATE " + GudangSysSchema + ".RcvOrderDetails " +
                                "SET SendFlag = 0, UpdatedOn=@UpdatedOn, Remark=@Remark, CustomField3='Submitted' WHERE " +
                                " TenantCode = @TenantCode AND OrderNumber =@OrderNumber AND CASENUMBER  LIKE '%" + row.CASENUMBER + "%' " +
                                " AND (SendFlag = 1)";
                        updateCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
                        updateCmd.Parameters.AddWithValue("@OrderNumber".ToUpper(), row.ORDERNUMBER.Trim());
                        updateCmd.Parameters.AddWithValue("@UpdatedOn", datestamp);
                        updateCmd.Parameters.AddWithValue("@Remark", JobId);
                        Db2GetDataTable(updateCmd, Db2GudangSysConnection());

                        counter = counter + 1;
                        JobId = "";
                        processFlag = true;
                    }
                }

            }


            //if (processFlag) ftp(ConfigurationManager.AppSettings["BC_Command"].ToString());
        }

        private static void iP1BCEXIP(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {
            string TopasDateString = datestamp.ToString("yyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            string TopasTimeString = DateTime.Now.ToString("HHmm", System.Globalization.CultureInfo.InvariantCulture);
            string JobId = "";
            int ShipCount = 0;
            int DenialCount = 0;

            iDB2Command sqlCmd = new iDB2Command();
            sqlCmd.CommandText = "SELECT TenantCode, WarehouseCode, DeliveryNo, ShipperName, ShipperAddress, " +
                "Route, VehicleNo, JobNo, srd.Reference " +
                "FROM  " + GudangSysSchema + ".SalesRouteDelivery AS srd " +
                "WHERE srd.Route LIKE 'EX%' AND srd.Reference = 'Submit' ";
            DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

            List<SalesRouteDelivery> gudangsyslist = new List<SalesRouteDelivery>();
            gudangsyslist = ConvertDataTable<SalesRouteDelivery>(gudangsysdata);

            foreach (SalesRouteDelivery row in gudangsyslist)
            {

                //remove error job data
                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "delete " + TopasSchema + ".P1BCEXIP where EHJBID ='" + row.DELIVERYNO + "'";
                Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "delete " + TopasSchema + ".P1BCEX2P where EXJBID ='" + row.DELIVERYNO + "'";
                Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "delete " + TopasSchema + ".P1BCEX3P where EXJBID ='" + row.DELIVERYNO + "'";
                Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                JobId = row.DELIVERYNO;
                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "SELECT srd.DeliveryNo AS EXJBID, sod.Key1 AS EXBHNO, ,sod.Key2 AS EXBITM,sod.LotNumber AS EXFRAN, " +
                    "sod.ItemCode AS EXPNO, SUM(satx.PackOkQty) AS EXPQTY, cs.CaseNumber AS EXCASE,srd.VehicleNo AS EXVSNM, srd.Origin AS EXCNTN, " +
                    "0 AS EXNETW,0 AS EXGRWT," +
                    "0 AS EXMEAS, '(' || cast(ctr.Height AS VARCHAR(10)) || 'x' || cast(ctr.width AS VARCHAR(10)) || 'x' || cast(ctr.Depth AS VARCHAR(10)) || ')cm' AS EXMCAS, " +
                    "VARCHAR_FORMAT(srd.ETA, 'YYYYMMDD') AS EXETAD , VARCHAR_FORMAT(srd.ETD, 'YYYYMMDD') AS EXSHPD , '' AS EXSTS FROM " + GudangSysSchema + ".SalesRouteDelivery AS srd " +
                    "INNER JOIN " + GudangSysSchema + ".CaseRef AS cs ON srd.WarehouseCode = cs.WarehouseCode AND srd.DeliveryNo = cs.DeliveryNo INNER JOIN " + GudangSysSchema + ".SalesTx AS stx ON cs.CaseRefId = stx.CaseRefId " +
                    "INNER JOIN " + GudangSysSchema + ".SalesAllocTx AS satx ON stx.SalesTxId = satx.SalesTxId INNER JOIN " + GudangSysSchema + ".SalesAlloc AS sa ON satx.SalesAllocId = sa.SalesAllocId " +
                    "INNER JOIN " + GudangSysSchema + ".SalesOrderDetails AS sod ON sa.SalesLineId = sod.SalesOrderDetailId INNER JOIN " + GudangSysSchema + ".CaseLabelRef AS clr ON cs.CaseRefId = clr.CaseRefId " +
                    "INNER JOIN " + GudangSysSchema + ".CaseTypeRef AS ctr ON clr.CaseType = ctr.TypeCode " +
                    "WHERE srd.DeliveryNo = @DeliveryNo AND  (sod.IsArchived = 0) AND sod.StatusCode='C' AND reference = 'Submit' " +
                    "GROUP BY srd.DeliveryNo,sod.Key1,sod.Key2,sod.LotNumber ,sod.ItemCode, cs.CaseNumber, srd.VehicleNo,srd.Origin," +
                    "'(' || cast(ctr.Height AS VARCHAR(10)) || 'x' || cast(ctr.width AS VARCHAR(10)) || 'x' || cast(ctr.Depth AS VARCHAR(10)) || ')cm'," +
                    "VARCHAR_FORMAT(srd.ETA, 'YYYYMMDD'),VARCHAR_FORMAT(srd.ETD, 'YYYYMMDD') ";
                sqlCmd.Parameters.AddWithValue("@DeliveryNo".ToUpper(), row.DELIVERYNO.Trim());
                DataTable Shipdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                List<P1BCEX2P> Shiplist = new List<P1BCEX2P>();
                Shiplist = ConvertDataTable<P1BCEX2P>(Shipdata);

                List<P1BCEX3P> Deniallist = new List<P1BCEX3P>();
                foreach (var i in Shiplist)
                {
                    sqlCmd = new iDB2Command();
                    sqlCmd.CommandText = "SELECT '' AS EXJBID, sod.Key1 AS EXBHNO, sod.Key2 AS EXBITM, " +
                        "sod.LotNumber AS EXFRAN, sod.ItemCode AS EXPNO , sod.PickLeQty +sod.PackLeQty AS EXDQTY, " +
                        "denial.Reason AS EXRSCD,'' AS EXDSTS FROM " + GudangSysSchema + ".SalesOrderDetails AS sod INNER JOIN  " +
                        "" + GudangSysSchema + ".SalesOrderTx AS sotx ON sod.SalesOrderDetailId = sotx.SalesOrderDetailId INNER JOIN  " +
                        "" + GudangSysSchema + ".DenialReason AS denial ON sotx.DenialReasonId = denial.id WHERE (sod.Key1 = " + i.EXBHNO + ") AND (sod.Key2 = " + i.EXBITM + ") " +
                        "AND sod.PickLeQty +sod.PackLeQty>0";

                    DataTable denialdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                    List<P1BCEX3P> t_deniallist = new List<P1BCEX3P>();
                    t_deniallist = ConvertDataTable<P1BCEX3P>(denialdata);
                    Deniallist.AddRange(t_deniallist);
                }


                ShipCount = Shiplist.Count();

                DenialCount = Deniallist.Count();
                int countrecords = ShipCount + DenialCount;
                string EHVSNM = Shiplist.Take(1).SingleOrDefault().EXVSNM;
                if (ShipCount + DenialCount > 0)
                {
                    //Single statement insert

                    foreach (var k in Shiplist)
                    {
                        iDB2Command tupdateCmd = new iDB2Command();
                        tupdateCmd.CommandText = "INSERT INTO P1BCEX2P ( EXJBID, EXBHNO, EXBITM, EXFRAN, EXPNO, EXPQTY, EXCASE, " +
                            "EXVSNM, EXCNTN, EXNETW, EXGRWT, EXMEAS, EXMCAS, EXETAD, EXSHPD, EXSTS)  VALUES " +
                            "( '" + JobId + "', '" + k.EXBHNO + "','" + k.EXBITM + "', '" + k.EXFRAN + "', '" + k.EXPNO + "', '" + k.EXPQTY + "', " +
                            "'" + k.EXCASE + "', '" + k.EXVSNM + "', '" + k.EXCNTN + "', '" + k.EXNETW + "','" + k.EXGRWT + "', '" + k.EXMEAS + "', " +
                            "'" + k.EXMCAS + "', '" + k.EXETAD + "', '" + k.EXSHPD + "', '" + k.EXSTS + "')";
                        Db2GetDataTable(tupdateCmd, Db2TopasConnection());
                    }

                    foreach (var k in Deniallist)
                    {
                        iDB2Command tupdateCmd = new iDB2Command();
                        tupdateCmd.CommandText = "INSERT INTO P1BCEX3P ( EXJBID, EXBHNO, EXBITM, EXFRAN, EXPNO, EXDQTY, EXRSCD, EXDSTS ) VALUES " +
                            " (  '" + JobId + "', '" + k.EXBHNO + "', '" + k.EXBITM + "', '" + k.EXFRAN + "', '" + k.EXPNO + "', '" + k.EXDQTY + "', '" + k.EXRSCD + "', '" + k.EXDSTS + "' )";
                        Db2GetDataTable(tupdateCmd, Db2TopasConnection());
                    }


                    iDB2Command updateCmd = new iDB2Command();
                    updateCmd.CommandText = "INSERT INTO P1BCEXIP ( EHJBID, EHVSNM, EHNRC2, EHNRC3, EHFLAG, EHCRDT, EHCRTM, EHUSER, EHUPDT, EHUPTM) VALUES " +
                        "(  '" + JobId + "', '" + EHVSNM + "', '" + ShipCount.ToString() + "','" + DenialCount.ToString() + "', '', '" + TopasDateString + "', '" + TopasTimeString + "', 'TBIPS', '" + TopasDateString + "', '" + TopasTimeString + "')" +
                    Db2GetDataTable(updateCmd, Db2TopasConnection());


                    updateCmd = new iDB2Command();
                    updateCmd.CommandText = "UPDATE " + GudangSysSchema + ".SalesRouteDelivery " +
                            "SET Reference = 'Sent' WHERE DeliveryNo= '" + JobId + "' ";

                    Db2GetDataTable(updateCmd, Db2GudangSysConnection());


                }
            }
            if (gudangsyslist.Count() > 0)
            {
                try
                {
                    ExecuteCommand(ConfigurationManager.AppSettings["Export_Command"].ToString());
                }
                catch (Exception e)
                {
                }
            }
            //end
        }

        private static void iP1BCCHP(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {
            string TopasDateString = datestamp.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            string TopasTimeString = DateTime.Now.ToString("HHmmss", System.Globalization.CultureInfo.InvariantCulture);
            string JobId = "";
            int ShipCount = 0;
            int DenialCount = 0;
            string customercode = "";


            iDB2Command sqlCmd = new iDB2Command();
            sqlCmd.CommandText = "SELECT TenantCode, WarehouseCode, DeliveryNo, ShipperName, ShipperAddress, " +
                "Route, VehicleNo, JobNo, srd.Reference " +
                "FROM  " + GudangSysSchema + ".SalesRouteDelivery AS srd " +
                "WHERE srd.Route  in ('DOMESTIC','RTI')  AND srd.Reference in ('Submit','Error') ";
            DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
            List<SalesRouteDelivery> gudangsyslist = new List<SalesRouteDelivery>();
            gudangsyslist = ConvertDataTable<SalesRouteDelivery>(gudangsysdata);



            foreach (SalesRouteDelivery row in gudangsyslist)
            {
                //remove error job data
                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "delete " + TopasSchema + ".P1BCCHP where CHJBID ='" + row.DELIVERYNO + "'";
                Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "delete " + TopasSchema + ".P1BCC2P where CDJBID ='" + row.DELIVERYNO + "'";
                Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "delete " + TopasSchema + ".P1BCC3P where CDJBID ='" + row.DELIVERYNO + "'";
                Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                JobId = row.DELIVERYNO;
                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "SELECT '" + row.DELIVERYNO.Trim() + "' AS CDJBID, sod.Key1 AS CDBHNO, sod.Key2 AS CDBITM,sod.LotNumber AS CDFRAN, " +
                    "sod.ItemCode AS CDPNO, sum(satx.PackOkQty) AS CDAQTY,  cr.CaseNumber AS CDCASE, " +
                    "'' AS CDFPNT, soh.CustomerCode as CDCUNO " +
                    "FROM  " + GudangSysSchema + ".SalesAllocTx AS satx INNER JOIN " +
                        " " + GudangSysSchema + ".SalesAlloc AS sa ON satx.SalesAllocId = sa.SalesAllocId INNER JOIN " +
                         " " + GudangSysSchema + ".SalesTx AS stx ON satx.SalesTxId = stx.SalesTxId INNER JOIN " +
                         "" + GudangSysSchema + ".CaseRef AS cr ON stx.CaseRefId = cr.CaseRefId INNER JOIN " +
                         "" + GudangSysSchema + ".SalesOrderHeaders AS soh INNER JOIN " +
                         "" + GudangSysSchema + ".SalesOrderDetails AS sod ON soh.TenantCode = sod.TenantCode " +
                         "AND soh.OrderNumber = sod.OrderNumber ON sa.SalesLineId = sod.SalesOrderDetailId " +
                         "where cr.DeliveryNo = @DeliveryNo and (sod.CustomField1 is null or sod.CustomField1 = '" + JobId + "')" +
                         "group by sod.Key1,sod.Key2,sod.LotNumber,sod.ItemCode ,cr.CaseNumber,soh.CustomerCode  ";
                //sqlCmd.CommandText = "SELECT '" + row.DELIVERYNO.Trim() + "' AS CDJBID, sod.Key1 AS CDBHNO, sod.Key2 AS CDBITM,sod.LotNumber AS CDFRAN, " +
                //    "sod.ItemCode AS CDPNO, sod.PackOkQty AS CDAQTY, '-' AS CDCASE, " +
                //    "'' AS CDFPNT, soh.CustomerCode as CDCUNO " +
                //    "FROM " + GudangSysSchema + ".SalesOrderHeaders AS soh " +
                //    "INNER JOIN " + GudangSysSchema + ".SalesOrderDetails AS sod ON soh.TenantCode = sod.TenantCode AND soh.OrderNumber = sod.OrderNumber " +
                //    "WHERE sod.SalesOrderDetailId IN (SELECT sa.SalesLineId FROM " + GudangSysSchema + ".CaseRef AS cr INNER JOIN " + GudangSysSchema + ".SalesTx AS stx " +
                //    "ON cr.CaseRefId = stx.CaseRefId INNER JOIN " + GudangSysSchema + ".SalesAllocTx AS satx ON stx.SalesTxId = satx.SalesTxId INNER JOIN " + GudangSysSchema + ".SalesAlloc AS sa " +
                //    "ON satx.SalesAllocId = sa.SalesAllocId WHERE cr.DeliveryNo = @DeliveryNo) and (sod.CustomField1 is null or sod.CustomField1 = '" + JobId + "')";
                sqlCmd.Parameters.AddWithValue("@DeliveryNo".ToUpper(), row.DELIVERYNO.Trim());
                DataTable Shipdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                List<P1BCC2P> Shiplist = new List<P1BCC2P>();
                Shiplist = ConvertDataTable<P1BCC2P>(Shipdata);

                List<P1BCC3P> Deniallist = new List<P1BCC3P>();
                var dlist = Shiplist.Select(c => new { CDBHNO = c.CDBHNO, CDCUNO = c.CDCUNO }).Distinct().ToList();
                foreach (var i in dlist)
                {
                    sqlCmd = new iDB2Command();
                    sqlCmd.CommandText = "SELECT distinct '' AS CDJBID, sod.Key1 AS CDBHNO, sod.Key2 AS CDBITM, " +
                        "sod.LotNumber AS CDFRAN, sod.ItemCode AS CDPNO , sod.PickLeQty +sod.PackLeQty AS CDDQTY, " +
                        "denial.Reason AS CDRSCD,'' AS CDFPNT FROM " + GudangSysSchema + ".SalesOrderDetails AS sod INNER JOIN  " +
                        "" + GudangSysSchema + ".SalesOrderTx AS sotx ON sod.SalesOrderDetailId = sotx.SalesOrderDetailId INNER JOIN  " +
                        "" + GudangSysSchema + ".SalesOrderHeaders AS soh ON soh.OrderNumber = sod.OrderNumber INNER JOIN  " +
                        "" + GudangSysSchema + ".DenialReason AS denial ON sotx.DenialReasonId = denial.id " +
                        "WHERE (sod.Key1 = '" + i.CDBHNO + "' and soh.CustomerCode ='" + i.CDCUNO + "') " +
                        // "AND (sod.Key2 = " + i.CDBITM + ") " +
                        "AND (sod.PickLeQty +sod.PackLeQty)>0 AND (sod.PickLeQty +sod.PackLeQty + sod.PackOkQty = sod.OrderGoQty)  " +
                        "AND (sod.CustomField1 is null or sod.CustomField1 = '" + JobId + "')";
                    DataTable denialdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                    List<P1BCC3P> t_deniallist = new List<P1BCC3P>();
                    t_deniallist = ConvertDataTable<P1BCC3P>(denialdata);

                    if (t_deniallist.Count() > 0) Deniallist.AddRange(t_deniallist);
                }


                ShipCount = Shiplist.Count();

                DenialCount = Deniallist.Count();
                int countrecords = ShipCount + DenialCount;

                if (ShipCount + DenialCount > 0)
                {
                    //Single statement insert
                    customercode = Shiplist.Take(1).SingleOrDefault().CDCUNO;
                    foreach (var k in Shiplist)
                    {
                        iDB2Command tupdateCmd = new iDB2Command();
                        tupdateCmd.CommandText = "INSERT INTO " + TopasSchema + ".P1BCC2P ( CDJBID, CDCASE, CDBHNO, CDBITM, CDFRAN, CDPNO, CDAQTY, CDFPNT ) VALUES " +
                            " ( '" + JobId + "', '" + k.CDCASE + "', '" + k.CDBHNO + "', '" + k.CDBITM + "', '" + k.CDFRAN + "','" + k.CDPNO + "', '" + k.CDAQTY + "', '" + k.CDFPNT + "' )";
                        Db2GetDataTable(tupdateCmd, Db2TopasConnection());
                        tupdateCmd = new iDB2Command();
                        tupdateCmd.CommandText = "UPDATE " + GudangSysSchema + ".SalesOrderDetails SET CustomField1 = '" + JobId + "'  WHERE Key1= '" + k.CDBHNO + "' AND Key2 = '" + k.CDBITM + "'";
                        Db2GetDataTable(tupdateCmd, Db2TopasConnection());
                    }

                    foreach (var k in Deniallist)
                    {

                        iDB2Command tupdateCmd = new iDB2Command();
                        tupdateCmd.CommandText = "INSERT INTO " + TopasSchema + ".P1BCC3P ( CDJBID, CDBHNO, CDBITM, CDFRAN, CDPNO, CDDQTY, CDRSCD, CDFPNT ) VALUES " +
                            " ( '" + JobId + "', '" + k.CDBHNO + "', '" + k.CDBITM + "', '" + k.CDFRAN + "','" + k.CDPNO + "', '" + k.CDDQTY + "', '" + k.CDRSCD + "', '" + k.CDFPNT + "'  )";
                        Db2GetDataTable(tupdateCmd, Db2TopasConnection());
                        tupdateCmd = new iDB2Command();
                        tupdateCmd.CommandText = "UPDATE " + GudangSysSchema + ".SalesOrderDetails SET CustomField1 = '" + JobId + "'  WHERE Key1= '" + k.CDBHNO + "' AND Key2 = '" + k.CDBITM + "'";
                        Db2GetDataTable(tupdateCmd, Db2TopasConnection());
                    }


                    iDB2Command supdateCmd = new iDB2Command();
                    supdateCmd.CommandText = "INSERT INTO " + TopasSchema + ".P1BCCHP ( CHJBID, CHMODE, CHCUNO, CHNRC2, CHNRC3, CHFLAG, CHCRDT, CHCRTM, CHUSER, CHUPDT, CHUPTM ) VALUES " +
                        "( '" + JobId + "', 'B', '" + customercode + "', '" + ShipCount.ToString() + "', '" + DenialCount.ToString() + "', '', '" + TopasDateString + "', '" + TopasTimeString + "', 'TBIPS', '" + TopasDateString + "','" + TopasTimeString + "' )";
                    Db2GetDataTable(supdateCmd, Db2TopasConnection());


                    supdateCmd = new iDB2Command();
                    supdateCmd.CommandText = "UPDATE " + GudangSysSchema + ".SalesRouteDelivery " +
                            "SET Reference = 'Sent' WHERE DeliveryNo= '" + JobId + "' ";

                    Db2GetDataTable(supdateCmd, Db2GudangSysConnection());
                }
            }
            if (gudangsyslist.Count() > 0)
            {
                try
                {
                    ExecuteCommand(ConfigurationManager.AppSettings["Domestic_Command"].ToString());
                }
                catch (Exception e)
                {
                }
            }
            //if (processFlag) ftp(ConfigurationManager.AppSettings["Domestic_Command"].ToString());
            //end
        }

        private static void iP1BCCHP_P2P(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {
            string TopasDateString = datestamp.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            string TopasTimeString = DateTime.Now.ToString("HHmmss", System.Globalization.CultureInfo.InvariantCulture);
            string JobId = "";
            int ShipCount = 0;
            int DenialCount = 0;
            string customercode = "";


            iDB2Command sqlCmd = new iDB2Command();
            sqlCmd.CommandText = "SELECT TenantCode, WarehouseCode, DeliveryNo, ShipperName, ShipperAddress, " +
                "Route, VehicleNo, JobNo, srd.Reference " +
                "FROM  " + GudangSysSchema + ".SalesRouteDelivery AS srd " +
                "WHERE srd.Route in ('P2P')  AND srd.Reference in ('Submit','Error') ";
            DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
            List<SalesRouteDelivery> gudangsyslist = new List<SalesRouteDelivery>();
            gudangsyslist = ConvertDataTable<SalesRouteDelivery>(gudangsysdata);

            foreach (SalesRouteDelivery row in gudangsyslist)
            {
                //remove error job data
                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "delete " + TopasSchema + ".P1BCCHP where CHJBID ='" + row.DELIVERYNO + "'";
                Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "delete " + TopasSchema + ".P1BCC2P where CDJBID ='" + row.DELIVERYNO + "'";
                Db2GetDataTable(sqlCmd, Db2GudangSysConnection());
                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "delete " + TopasSchema + ".P1BCC3P where CDJBID ='" + row.DELIVERYNO + "'";
                Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                JobId = row.DELIVERYNO;
                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "SELECT distinct srd.DeliveryNo AS CDJBID, sod.Key1 AS CDBHNO, sod.Key2 AS CDBITM,sod.LotNumber AS CDFRAN,sod.ItemCode AS CDPNO, " +
                "sod.LoadOkQty AS  CDAQTY, cr.CaseNumber AS CDCASE, ''  CDFPNT, cr.CustomerCode as CDCUNO " +
                "FROM " + GudangSysSchema + ".SalesRouteDelivery AS srd INNER JOIN " +
                 "" + GudangSysSchema + ".CaseRef AS cr ON srd.TenantCode = cr.ClientAccountCode AND srd.DeliveryNo = cr.DeliveryNo INNER JOIN " +
                 "" + GudangSysSchema + ".SalesOrderDetails AS sod ON cr.ClientAccountCode = sod.TenantCode AND cr.CaseNumber = sod.BatchNumber " +
                 "WHERE  srd.DeliveryNo = @DeliveryNo";
                sqlCmd.Parameters.AddWithValue("@DeliveryNo".ToUpper(), row.DELIVERYNO.Trim());
                DataTable Shipdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                List<P1BCC2P> Shiplist = new List<P1BCC2P>();
                Shiplist = ConvertDataTable<P1BCC2P>(Shipdata);

                //List<P1BCC3P> Deniallist = new List<P1BCC3P>();
                //var dlist = Shiplist.Select(c => new { CDBHNO = c.CDBHNO }).Distinct().ToList();
                //foreach (var i in dlist)
                //{
                //    sqlCmd = new iDB2Command();
                //    sqlCmd.CommandText = "SELECT '' AS CDJBID, sod.Key1 AS CDBHNO, sod.Key2 AS CDBITM, " +
                //        "sod.LotNumber AS CDFRAN, sod.ItemCode AS CDPNO , sod.PickLeQty +sod.PackLeQty AS CDDQTY, " +
                //        "denial.Reason AS CDRSCD,'' AS CDFPNT FROM " + GudangSysSchema + ".SalesOrderDetails AS sod INNER JOIN  " +
                //        "" + GudangSysSchema + ".SalesOrderTx AS sotx ON sod.SalesOrderDetailId = sotx.SalesOrderDetailId INNER JOIN  " +
                //        "" + GudangSysSchema + ".DenialReason AS denial ON sotx.DenialReasonId = denial.id WHERE (sod.Key1 = '" + i.CDBHNO + "') " +
                //        // "AND (sod.Key2 = " + i.CDBITM + ") " +
                //        "AND (sod.PickLeQty +sod.PackLeQty)>0";
                //    Console.WriteLine(sqlCmd.CommandText);
                //    DataTable denialdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                //    List<P1BCC3P> t_deniallist = new List<P1BCC3P>();
                //    t_deniallist = ConvertDataTable<P1BCC3P>(denialdata);

                //    if (t_deniallist.Count() > 0) Deniallist.AddRange(t_deniallist);
                //}


                ShipCount = Shiplist.Count();

                DenialCount = 0;
                int countrecords = ShipCount + DenialCount;

                if (ShipCount + DenialCount > 0)
                {
                    //Single statement insert
                    customercode = Shiplist.Take(1).SingleOrDefault().CDCUNO;
                    foreach (var k in Shiplist)
                    {
                        iDB2Command tupdateCmd = new iDB2Command();
                        tupdateCmd.CommandText = "INSERT INTO " + TopasSchema + ".P1BCC2P ( CDJBID, CDCASE, CDBHNO, CDBITM, CDFRAN, CDPNO, CDAQTY, CDFPNT ) VALUES " +
                            " ( '" + JobId + "', '" + k.CDCASE + "', '" + k.CDBHNO + "', '" + k.CDBITM + "', '" + k.CDFRAN + "','" + k.CDPNO + "', '" + k.CDAQTY + "', '" + k.CDFPNT + "' )";
                        Db2GetDataTable(tupdateCmd, Db2TopasConnection());
                    }

                    //foreach (var k in Deniallist)
                    //{

                    //    iDB2Command tupdateCmd = new iDB2Command();
                    //    tupdateCmd.CommandText = "INSERT INTO " + TopasSchema + ".P1BCC3P ( CDJBID, CDBHNO, CDBITM, CDFRAN, CDPNO, CDDQTY, CDRSCD, CDFPNT ) VALUES " +
                    //        " ( '" + JobId + "', '" + k.CDBHNO + "', '" + k.CDBITM + "', '" + k.CDFRAN + "','" + k.CDPNO + "', '" + k.CDDQTY + "', '" + k.CDRSCD + "', '" + k.CDFPNT + "'  )";
                    //    Db2GetDataTable(tupdateCmd, Db2TopasConnection());
                    //}


                    iDB2Command supdateCmd = new iDB2Command();
                    supdateCmd.CommandText = "INSERT INTO " + TopasSchema + ".P1BCCHP ( CHJBID, CHMODE, CHCUNO, CHNRC2, CHNRC3, CHFLAG, CHCRDT, CHCRTM, CHUSER, CHUPDT, CHUPTM ) VALUES " +
                        "( '" + JobId + "', 'B', '" + customercode + "', '" + ShipCount.ToString() + "', '" + DenialCount.ToString() + "', '', '" + TopasDateString + "', '" + TopasTimeString + "', 'TBIPS', '" + TopasDateString + "','" + TopasTimeString + "' )";
                    Db2GetDataTable(supdateCmd, Db2TopasConnection());


                    supdateCmd = new iDB2Command();
                    supdateCmd.CommandText = "UPDATE " + GudangSysSchema + ".SalesRouteDelivery " +
                            "SET Reference = 'Sent' WHERE DeliveryNo= '" + JobId + "' ";

                    Db2GetDataTable(supdateCmd, Db2GudangSysConnection());
                }
            }
            if (gudangsyslist.Count() > 0)
            {
                try
                {
                    ExecuteCommand(ConfigurationManager.AppSettings["Domestic_Command"].ToString());
                }
                catch (Exception e)
                {
                }
            }
            //if (processFlag) ftp(ConfigurationManager.AppSettings["Domestic_Command"].ToString());
            //end
        }

        private static void iP1FD01HP(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {
            string TopasDateString = datestamp.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            string TopasTimeString = DateTime.Now.ToString("HHmmss", System.Globalization.CultureInfo.InvariantCulture);
            string JobId = "";
            int ShipCount = 0;
            int DenialCount = 0;
            int OutstandingCount = 0;
            int counter = 1;
            bool processFlag = false;


            iDB2Command sqlCmd = new iDB2Command();
            sqlCmd.CommandText = "SELECT TenantCode, WarehouseCode, DeliveryNo, ShipperName, ShipperAddress, " +
                "Route, VehicleNo, JobNo, srd.Reference " +
                "FROM  " + GudangSysSchema + ".SalesRouteDelivery AS srd " +
                "WHERE srd.Route  in ('DOMESTIC','RTI')  AND srd.Reference in ('Submit','Error') ";

            //sqlCmd.CommandText = "SELECT trim(TenantCode) as TenantCode,trim(OrderNumber) as OrderNumber," +
            //    "OrderTypeCode as TypeCode, trim(StatusCode) as StatusCode ,trim(WarehouseCode) as WarehouseCode, trim(CustomerCode) as CustomerCode " +
            //    "FROM " + GudangSysSchema + ".SalesOrderHeaders " +
            //    "WHERE StatusCode = 'C' AND (OrderTypeCode like 'EM%')";
            DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

            List<SalesOrderHeaders2> gudangsyslist = new List<SalesOrderHeaders2>();
            gudangsyslist = ConvertDataTable<SalesOrderHeaders2>(gudangsysdata);

            foreach (SalesOrderHeaders2 row in gudangsyslist)
            {
                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "SELECT trim(StatusCode) as StatusCode, trim(IsArchived) as IsArchived, trim(PackOkQty) as PackOkQty, " +
                    "trim(PickLeQty) as PickLeQty, trim(PickQuQty) as PickQuQty, trim(CheckQuQty) as CheckQuQty , trim(PackQuQty) as PackQuQty  , trim(ShipOkQty) as ShipOkQty " +
                    "FROM " + GudangSysSchema + ".SalesOrderDetails " +
                    "WHERE CustomField1 = '" + row.DELIVERYNO + "'";
                //sqlCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), row.TENANTCODE.Trim());
                //sqlCmd.Parameters.AddWithValue("@OrderNumber".ToUpper(), row.ORDERNUMBER.Trim());
                DataTable Shipdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                List<SalesOrderDetails2> Shiplist = new List<SalesOrderDetails2>();
                Shiplist = ConvertDataTable<SalesOrderDetails2>(Shipdata);

                //OutstandingCount = Shiplist.Where(c => c.STATUSCODE == "N" && c.ISARCHIVED == "0").Count();
                //if (OutstandingCount > 0) break;

                ShipCount = Shiplist.Where(c => (Convert.ToInt32(c.PACKOKQTY) > 0  && c.ISARCHIVED == "0")).Count();

                DenialCount = Shiplist.Where(c => Convert.ToInt32(c.PICKLEQTY) + Convert.ToInt32(c.PICKQUQTY) > 0 && c.ISARCHIVED == "0").Count();
                int countrecords = Shiplist.Count();

                if (ShipCount + DenialCount > 0 && OutstandingCount == 0)
                {
                    //Single statement insert
                    JobId = TopasDateString + "-EM-" + TopasTimeString + "-" + counter.ToString();
                    iDB2Command updateCmd = new iDB2Command();
                    updateCmd.CommandText = "INSERT INTO " + TopasSchema + ".P1FD01TP " +
                            "(FTFLID, FTJBID, FTBHNO, FTBITM, FTCASE, FTFRAN, FTPNO, FTQTY, FTETAD, FTETAP, FTSPDT, FTGWGT, FTNWGT, FTMEAS, FTSINV, FTVSCD, FTCNTN, FTVNME, FTERCD) " +
                            "SELECT 'T','" + JobId + "',Key1,Key2,'K_0400', LotNumber,ItemCode,PackOkQty,'20180221','20180221','20180221'," +
                            " '145','145','0','K00558A','-', 'CAIU9930355','AT JADE V.18AJ017E',''" +
                            "  FROM " + GudangSysSchema + ".SalesOrderDetails " +
                    "WHERE TenantCode = '" + row.TENANTCODE.Trim() + "' AND OrderNumber = '" + row.ORDERNUMBER.Trim() + "' AND ShipOkQty  >0 " +
                    "AND StatusCode = 'C'";
                    Db2GetDataTable(updateCmd, Db2TopasConnection());


                    updateCmd = new iDB2Command();
                    updateCmd.CommandText = "INSERT INTO " + TopasSchema + ".P1FD013P " +
                            "(FTJBID, FTBHNO, FTBITM, FTFRAN, FTPNO, FTDNQT, FTRSCD, FTERCD) " +
                            "SELECT '" + JobId + "',Key1,Key2, LotNumber,ItemCode,PickLeQty,'D03',''  FROM " + GudangSysSchema + ".SalesOrderDetails " +
                    "WHERE TenantCode = '" + row.TENANTCODE.Trim() + "' AND OrderNumber = '" + row.ORDERNUMBER.Trim() + "' AND PickLeQty >0 AND StatusCode = 'C'";
                    Db2GetDataTable(updateCmd, Db2TopasConnection());

                    updateCmd = new iDB2Command();
                    updateCmd.CommandText = "INSERT INTO " + TopasSchema + ".P1FD01HP " +
                    "(FHFLID, FHJBID, FHSINV, FHNRC2, FHNRC3, FHFLAG, FHCRDT, FHCRTM, FHUSER, FHUPDT, FHUPTM)  " +
                    "VALUES  ('T','" + JobId + "', 'K00558','" + ShipCount.ToString() + "','" + DenialCount.ToString() + "','','" + TopasDate(datestamp) + "', '" + TopasTime(datestamp) + "','TBIPS','" + TopasDate(datestamp) + "', '" + TopasTime(datestamp) + "') ";
                    Db2GetDataTable(updateCmd, Db2TopasConnection());

                    counter = counter + 1;

                    //updateCmd = new iDB2Command();
                    //updateCmd.CommandText = "UPDATE " + GudangSysSchema + ".SalesOrderDetails " +
                    //        "SET StatusCode = 'S' WHERE TenantCode = @TenantCode AND OrderNumber =@OrderNumber  and StatusCode = 'C' ";

                    updateCmd.Parameters.AddWithValue("@TenantCode".ToUpper(), ClientAccount);
                    updateCmd.Parameters.AddWithValue("@OrderNumber".ToUpper(), row.ORDERNUMBER.Trim());
                    Db2GetDataTable(updateCmd, Db2GudangSysConnection());

                    RunCmd(ConfigurationManager.AppSettings["EM_Command"].ToString(), Db2TopasConnection());
                    processFlag = true;
                }
            }

            //end
        }

        private static void iP1LOC2P(string TopasSchema, string GudangSysSchema, string ClientAccount, string TopasSQL, string GSysSQL, DateTime datestamp, string WarehouseCode)
        {
            string TopasDateString = datestamp.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            string TopasTimeString = DateTime.Now.ToString("HHmmss", System.Globalization.CultureInfo.InvariantCulture);

            iDB2Command sqlCmd = new iDB2Command();
            sqlCmd.CommandText = "select * from " + GudangSysSchema + ".LocationAssignment WHERE Status = 'Submitted' and Jobid <> ''";
            DataTable gudangsysdata = Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

            List<LocationAssignment> gudangsyslist = new List<LocationAssignment>();
            gudangsyslist = ConvertDataTable<LocationAssignment>(gudangsysdata);

            foreach (LocationAssignment row in gudangsyslist)
            {
                //temporary 
                row.NOOFBIN = "1";
                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "Insert into " + TopasSchema + ".P1LOC2P (L2JBID,L2FRAN,L2PNO,L2OLOC,L2NLOC,L2OBOX,L2NBOX,L2OQBX,L2NQBX,L2FLAG,L2FPNT,L2CRDT,L2CRTM,L2USER,L2UPDT,L2UPTM) VALUES " +
                    "('" + row.JOBID + "','" + row.LOTNUMBER + "','" + row.ITEMCODE + "'," +
                    "'" + row.OLDLOCATION + "','" + row.LOCATIONCODE + "','" + row.OLDNOOFBIN + "','" + row.NOOFBIN + "','" + row.OLDSTANDARDQUANTITY + "'," +
                    "'" + row.STANDARDQUANTITY + "','','','" + TopasDateString + "','" + TopasTimeString + "','" + row.UPDATEDBY + "','" + TopasDateString + "','" + TopasTimeString + "')";


                Db2GetDataTable(sqlCmd, Db2GudangSysConnection());

                sqlCmd = new iDB2Command();
                sqlCmd.CommandText = "UPDATE " + GudangSysSchema + ".LocationAssignment SET Status='Sent' where requestid ='" + row.REQUESTID + "'";
                Db2GetDataTable(sqlCmd, Db2GudangSysConnection());


            }
            if (gudangsyslist.Count() > 0)
            {

                try
                {
                    ExecuteCommand(ConfigurationManager.AppSettings["Location_Command"].ToString());
                }
                catch (Exception e)
                {
                }
            }



            //end
        }


        public class P1CSUPP
        {
            public string TSVEND { get; set; }
            public string TSSNAM { get; set; }
            public string TSADDR1 { get; set; }
            public string TSADDR2 { get; set; }
            public string TSADDR3 { get; set; }
            public string TSVNTY { get; set; }
            public string TSDTFR { get; set; }
            public string TSDTTO { get; set; }
        }

        public class P1BUYCP
        {
            public string BYBYCD { get; set; }
            public string BYBBBC { get; set; }
            public string BYSLCD { get; set; }
            public string BYBYNM { get; set; }
            public string BYADR1 { get; set; }
            public string BYADR2 { get; set; }
            public string BYADR3 { get; set; }
            public string BYCOUN { get; set; }
            public string BYCONP { get; set; }
            public string BYSLIN { get; set; }
            public string BYSPRE { get; set; }
            public string BYSSRN { get; set; }
            public string BYAPRE { get; set; }
            public string BYASRN { get; set; }
            public string BYBSNM { get; set; }
            public string BYSMRK { get; set; }
        }

        public class P1GIM0P
        {
            public string G0CONM { get; set; }
            public string G0ADR1 { get; set; }
            public string G0ADR2 { get; set; }
            public string G0ADR3 { get; set; }
        }

        public class P1CUSTP
        {
            public string CMCMNO { get; set; }
            public string CMCMEN { get; set; }
            public string CMADR1 { get; set; }
            public string CMADR2 { get; set; }
            public string REFCAT { get; set; }
            public string REFKEY { get; set; }
            public string REFDAT { get; set; }
        }

        public class REFERP
        {
            public string REFCAT { get; set; }
            public string REFKEY { get; set; }
            public string REFDAT { get; set; }
        }

        public class P1PARTP
        {
            public string PMPNO { get; set; }
            public string PMPNME { get; set; }
            public string PMFRAN { get; set; }
            public string PMWGTP { get; set; }
            public string PMHIGP { get; set; }
            public string PMWIDP { get; set; }
            public string PMDEPP { get; set; }
        }

        public class P1PMSTP
        {
            public string PMBRCD { get; set; }
            public string PMPNO { get; set; }
            public string PMFRAN { get; set; }
            public string PMLOC { get; set; }
            public string PMLANC { get; set; }
        }

        public class P2ELOCP
        {
            public string XXLOC { get; set; }
            public string XXBINT { get; set; }
            public string XXPGRP { get; set; }
            public string XXBINL { get; set; }
            public string XXBINH { get; set; }
            public string XXBINW { get; set; }
            public string XXRACK { get; set; }
            public string XXZONE { get; set; }
        }

        public class P1BCBHP
        {
            public string BHBHNO { get; set; }
            public string BHSHPT { get; set; }
            public string BHINVO { get; set; }
            public string BHVEND { get; set; }
            public string BHBUYF { get; set; }
            public string BHTYPE { get; set; }
            public string BHNRCD { get; set; }
            public string BHFLAG { get; set; }
            public string BHCRDT { get; set; }
            public string BHCRTM { get; set; }
            public string BHUSER { get; set; }
            public string BHUPDT { get; set; }
            public string BHUPTM { get; set; }
        }

        public class P1BCBIP
        {
            public string BSBHNO { get; set; }
            public string BSBITM { get; set; }
            public string BSTYPE { get; set; }
            public string BSCASE { get; set; }
            public string BSFRAN { get; set; }
            public string BSPNO { get; set; }
            public string BSODTP { get; set; }
            public string BSIQTY { get; set; }
            public string BSZONE { get; set; }
            public string BSLOC { get; set; }
            public string BSIVNO { get; set; }
            public string BSCRDT { get; set; }
            public string BSCRTM { get; set; }
            public string BSUSER { get; set; }
            public string BSUPDT { get; set; }
            public string BSUPTM { get; set; }
            public string BSFLAG { get; set; }
            public string BSIMPT { get; set; }
            public string BSREMK { get; set; }
            public string BSPGMN { get; set; }
            public string BSROUT { get; set; }
            public string BSCUNO { get; set; }
            public string BSORDN { get; set; }
            public string BSPDAT { get; set; }
            public string BSITEM { get; set; }
            public string BSSUFX { get; set; }
            public string BSOINV { get; set; }
            public string BSPUOR { get; set; }
            public string BSBCNO { get; set; }
            public string BSBCCD { get; set; }
            public string BSINV2 { get; set; }
            public string BSOIV2 { get; set; }
            public string BSSPRC { get; set; }
        }

        public class P1BCEXHP
        {
            public string BHBHNO { get; set; }
            public string BHNRCD { get; set; }
            public string BHFLAG { get; set; }
            public string BHCRDT { get; set; }
            public string BHCRTM { get; set; }
            public string BHUSER { get; set; }
            public string BHUPDT { get; set; }
            public string BHUPTM { get; set; }
        }

        public class P1BCEX1P
        {
            public string EXBHNO { get; set; }
            public string EXBITM { get; set; }
            public string EXPDAT { get; set; }
            public string EXFRAN { get; set; }
            public string EXPNO { get; set; }
            public string EXORDN { get; set; }
            public string EXTREF { get; set; }
            public string EXODTP { get; set; }
            public string EXTAXE { get; set; }
            public string EXBOFL { get; set; }
            public string EXZONE { get; set; }
            public string EXITEM { get; set; }
            public string EXLOC { get; set; }
            public string EXPINV { get; set; }
            public string EXIQTY { get; set; }
            public string EXPNME { get; set; }
            public string EXBPCD { get; set; }
            public string EXTRAN { get; set; }
            public string EXRMRK { get; set; }
            public string EXETDT { get; set; }
            public string EXSTS { get; set; }
        }

        public class P1BCPHP
        {
            public string BHBHNO { get; set; }
            public string BHMODE { get; set; }
            public string BHNRCD { get; set; }
            public string BHFLAG { get; set; }
            public string BHCRDT { get; set; }
            public string BHCRTM { get; set; }
            public string BHUSER { get; set; }
            public string BHUPDT { get; set; }
            public string BHUPTM { get; set; }
        }

        public class P1BCPIP
        {
            public string BPBHNO { get; set; }
            public string BPBITM { get; set; }
            public string BPSINV { get; set; }
            public string BPTYPE { get; set; }
            public string BPROUT { get; set; }
            public string BPZONE { get; set; }
            public string BPITEM { get; set; }
            public string BPLOC { get; set; }
            public string BPFRAN { get; set; }
            public string BPPNO { get; set; }
            public string BPAQTY { get; set; }
            public string BPINVN { get; set; }
            public string BPCUNO { get; set; }
            public string BPCTYP { get; set; }
            public string BPORDN { get; set; }
            public string BPTREF { get; set; }
            public string BPODTP { get; set; }
            public string BPCASE { get; set; }
            public string BPPDAT { get; set; }
            public string BPCRDT { get; set; }
            public string BPCRTM { get; set; }
            public string BPUSER { get; set; }
            public string BPUPDT { get; set; }
            public string BPUPTM { get; set; }
            public string BPFLAG { get; set; }
            public string BPREMK { get; set; }
            public string BPPGMN { get; set; }
            public string BPSPRC { get; set; }
        }

        public class P1STOCHP
        {
            public string SHJBID { get; set; }
            public string SHNRCD { get; set; }
            public string SHBQTY { get; set; }
            public string SHFLAG { get; set; }
            public string SHCRDT { get; set; }
            public string SHCRTM { get; set; }
            public string SHUSER { get; set; }
            public string SHUPDT { get; set; }
            public string SHUPTM { get; set; }
        }

        public class P1STOC1P
        {
            public string STFLID { get; set; }
            public string STSERN { get; set; }
            public string STFRAN { get; set; }
            public string STPNO { get; set; }
            public string STLANC { get; set; }
            public string STOHB { get; set; }
            public string STOHP { get; set; }
            public string STLOCB { get; set; }
            public string STLOCP { get; set; }
            public string STFLG1 { get; set; }
            public string STBLKF { get; set; }
        }

        public class P1MOVIP
        {
            public string BPLOCU { get; set; }
            public string BPINVN { get; set; }
            public string BPITEM { get; set; }
        }

        public class P1MOV2P
        {
            public string BPTYPE { get; set; }
            public string BPINVN { get; set; }
            public string BPITEM { get; set; }
            public string BPLOC { get; set; }
            public string BPFRAN { get; set; }
            public string BPPNO { get; set; }
            public string BPQTY { get; set; }
            public string BPCRDT { get; set; }
            public string BPCRTM { get; set; }
            public string BPUSER { get; set; }
            public string BPUPDT { get; set; }
            public string BPUPTM { get; set; }
            public string BHFLAG { get; set; }
        }

        public class SalesOrderHeaders
        {
            public string TenantCode { get; set; }
            public string OrderNumber { get; set; }
            public string WarehouseCode { get; set; }
            public string OrderTypeCode { get; set; }
            public string CustomerCode { get; set; }
            public string CustomerName { get; set; }
            public string CustomerOrderNumber { get; set; }
            public string ShippingAddress { get; set; }
            public string BillToShippingAddress { get; set; }
            public string BillingAddress { get; set; }
            public string DeliveryDate { get; set; }
            public string Remarks { get; set; }
            public string StatusCode { get; set; }
            public string RevisionNumber { get; set; }
            public string CreatedBy { get; set; }
            public string CreatedOn { get; set; }
            public string UpdatedBy { get; set; }
            public string UpdatedOn { get; set; }
            public string IsArchived { get; set; }
        }

        public class SalesOrderDetails
        {
            public string TenantCode { get; set; }
            public string OrderNumber { get; set; }
            public string SalesOrderDetailId { get; set; }
            public string ItemCode { get; set; }
            public string ItemUomCode { get; set; }
            public string UomLooseQty { get; set; }
            public string ItemPrice { get; set; }
            public string BatchNumber { get; set; }
            public string LotNumber { get; set; }
            public string ExpiryDate { get; set; }
            public string RevisionNumber { get; set; }
            public string OrderGoQty { get; set; }
            public string AllocateOkQty { get; set; }
            public string AllocateBoQty { get; set; }
            public string PickGoQty { get; set; }
            public string PickOkQty { get; set; }
            public string PickLeQty { get; set; }
            public string PickQuQty { get; set; }
            public string CheckOkQty { get; set; }
            public string CheckLeQty { get; set; }
            public string CheckQuQty { get; set; }
            public string PackOkQty { get; set; }
            public string PackLeQty { get; set; }
            public string PackQuQty { get; set; }
            public string LoadOkQty { get; set; }
            public string ShipOkQty { get; set; }
            public string PendingShipQty { get; set; }
            public string CreatedBy { get; set; }
            public string CreatedOn { get; set; }
            public string UpdatedBy { get; set; }
            public string UpdatedOn { get; set; }
            public string IsArchived { get; set; }
            public string Remark { get; set; }
            public string IsPickItem { get; set; }
            public string NewFlag { get; set; }
            public string AlertFlag { get; set; }
            public string GRBinnedLooseQty { get; set; }
            public string LocationCode { get; set; }
            public string AllocationType { get; set; }
            public string CrossDockFlag { get; set; }
            public string LineNumber { get; set; }
            public string PalletBatch { get; set; }
            public string PalletNumber { get; set; }
            public string Key1 { get; set; }
            public string Key2 { get; set; }
            public string StatusCode { get; set; }
            public bool SendFlag { get; set; }
        }

        public class SalesOrderHeaders2
        {
            public string TENANTCODE { get; set; }
            public string ORDERNUMBER { get; set; }
            public string WAREHOUSECODE { get; set; }
            public string ORDERTYPECODE { get; set; }
            public string STATUSCODE { get; set; }
            public string CUSTOMERCODE { get; set; }
            public string DELIVERYNO { get; set; }
        }

        public class SalesOrderDetails2
        {
            public string TENANTCODE { get; set; }
            public string ORDERNUMBER { get; set; }
            public string ITEMCODE { get; set; }
            public string LOTNUMBER { get; set; }
            public string ORDERGOQTY { get; set; }
            public string PICKLEQTY { get; set; }
            public string PICKQUQTY { get; set; }
            public string PACKOKQTY { get; set; }
            public string ISARCHIVED { get; set; }
            public string KEY1 { get; set; }
            public string KEY2 { get; set; }
            public string STATUSCODE { get; set; }
            public bool SENDFLAG { get; set; }
        }

        public class SupplierMaster
        {
            public string TenantCode { get; set; }
            public string SupplierCode { get; set; }
            public string SupplierName { get; set; }
            public string Address { get; set; }
            public string CreatedBy { get; set; }
            public string CreatedOn { get; set; }
            public string UpdatedBy { get; set; }
            public string UpdatedOn { get; set; }
            public string IsArchived { get; set; }
            public string SupplierTypeCode { get; set; }
            public string ContactPerson { get; set; }
            public string ContackNumber { get; set; }
            public string EmailAddress { get; set; }
            public string FaxNumber { get; set; }
        }

        public class TenantMaster
        {
            public string TenantCode { get; set; }
            public string TenantName { get; set; }
            public string Address { get; set; }
            public string ContactPerson { get; set; }
            public string ContactNumber { get; set; }
            public string FaxNumber { get; set; }
            public string Email { get; set; }
            public string Logo { get; set; }
            public string ColourCode { get; set; }
            public string Currency { get; set; }
            public string CreatedBy { get; set; }
            public string CreatedOn { get; set; }
            public string UpdatedBy { get; set; }
            public string UpdatedOn { get; set; }
            public string IsArchived { get; set; }
        }

        public class CustomerMaster
        {
            public string TenantCode { get; set; }
            public string CustomerCode { get; set; }
            public string CustomerName { get; set; }
            public string InterfaceCustomerCode { get; set; }
            public string CustomerTypeCode { get; set; }
            public string ShippingAddress { get; set; }
            public string BillToShippingAddress { get; set; }
            public string BillingAddress { get; set; }
            public string ContactPerson { get; set; }
            public string ContactNumber { get; set; }
            public string FaxNumber { get; set; }
            public string Email { get; set; }
            public string SalesPersonInCharge { get; set; }
            public string CreatedBy { get; set; }
            public string CreatedOn { get; set; }
            public string UpdatedBy { get; set; }
            public string UpdatedOn { get; set; }
            public string IsArchived { get; set; }
            public string Status { get; set; }
            public string DefaultAllocation { get; set; }
        }

        public class DeliveryAddress
        {
            public string TenantCode { get; set; }
            public string CustomerCode { get; set; }
            public string AddressName { get; set; }
            public string Address { get; set; }
            public string isArchived { get; set; }
            public string CreatedBy { get; set; }
            public string CreatedOn { get; set; }
            public string UpdatedBy { get; set; }
            public string UpdatedOn { get; set; }
        }

        public class WHBinTypeRef
        {
            public string WarehouseCode { get; set; }
            public string TypeCode { get; set; }
            public string Description { get; set; }
            public string Height { get; set; }
            public string Width { get; set; }
            public string Depth { get; set; }
            public string Capacity { get; set; }
            public string EffectiveVolumePct { get; set; }
            public string ColourCode { get; set; }
            public string BinWeight { get; set; }
            public string ColumnWeight { get; set; }
            public string CreatedBy { get; set; }
            public string CreatedOn { get; set; }
            public string UpdatedBy { get; set; }
            public string UpdatedOn { get; set; }
            public string IsArchived { get; set; }
        }

        public class ItemMaster
        {
            public string TenantCode { get; set; }
            public string ItemCode { get; set; }

        }

        public class WHLocationRef
        {
            public string WarehouseCode { get; set; }
            public string LocationCode { get; set; }
            public string InterfaceLocationCode { get; set; }
            public string ZoneCode { get; set; }
            public string RackNumber { get; set; }
            public string ColumnNumber { get; set; }
            public string LevelNumber { get; set; }
            public string BinNumber { get; set; }
            public string BinTypeCode { get; set; }
            public string IsLocked { get; set; }
            public string CreatedBy { get; set; }
            public string CreatedOn { get; set; }
            public string UpdatedBy { get; set; }
            public string UpdatedOn { get; set; }
            public string IsArchived { get; set; }
            public string LooseNotAllowed { get; set; }
            public string PickFace { get; set; }
            public string PriorityFlag { get; set; }
            public string PriorityNumber { get; set; }
        }

        public class WHZoneTypeRef
        {
            public string WarehouseCode { get; set; }
            public string TypeCode { get; set; }
            public string Description { get; set; }
            public string ExcludeStockCount { get; set; }
            public string ExcludePicking { get; set; }
            public string CrossDockFlag { get; set; }
            public string ColourCode { get; set; }
        }

        public class ItemRecommendedWHLocationRef
        {
            public string TenantCode { get; set; }
            public string ItemCode { get; set; }
            public string WarehouseCode { get; set; }
            public string ZoneCode { get; set; }
            public string LocationCode { get; set; }
            public string IsArchived { get; set; }
        }

        public class ItemUomMaster
        {
            public string TenantCode { get; set; }
            public string ItemCode { get; set; }
            public string UomCode { get; set; }
            public string PackSizeBarcode { get; set; }
            public string LooseQty { get; set; }
            public string Description { get; set; }
            public string ItemPrice { get; set; }
            public string Height { get; set; }
            public string Width { get; set; }
            public string Depth { get; set; }
            public string Weight { get; set; }
            public string Capacity { get; set; }
            public string CreatedBy { get; set; }
            public string CreatedOn { get; set; }
            public string UpdatedBy { get; set; }
            public string UpdatedOn { get; set; }
            public string IsArchived { get; set; }
        }

        public class RcvOrderHeaders
        {
            public string TENANTCODE { get; set; }
            public string ORDERNUMBER { get; set; }
            public string TYPECODE { get; set; }
            public string STATUSCODE { get; set; }
            public string WAREHOUSECODE { get; set; }
            public string REMARK { get; set; }
            public string CASENUMBER { get; set; }
        }

        public class ERPDailyInventory_Snapshot
        {
            public string DateStamp { get; set; }
            public string ClientAccount { get; set; }
            public string ItemNo { get; set; }
            public string BatchNumber { get; set; }
            public string LotNumber { get; set; }
            public string Qty { get; set; }
            public string StockCountId { get; set; }
            public string WarehouseCode { get; set; }
            public string LocationCode { get; set; }
        }

        public class ERP_Integration
        {
            public string id { get; set; }
            public string Description { get; set; }
            public string Value { get; set; }
            public string UpdatedOn { get; set; }
            public string ClientAccount { get; set; }
        }

        public class RcvOrderDetails
        {
            public string TENANTCODE { get; set; }
            public string ORDERNUMBER { get; set; }
            public string CASENUMBER { get; set; }
            public string ITEMCODE { get; set; }
            public string LOTNUMBER { get; set; }
            public string BINOKQTY { get; set; }
            public string BINLEQTY { get; set; }
            public string CHECKLEQTY { get; set; }
            public string BINQUQTY { get; set; }
            public string STATUSCODE { get; set; }
            public string ISARCHIVED { get; set; }
            public string ISCHECKITEM { get; set; }
            public string KEY1 { get; set; }
            public string KEY2 { get; set; }
            public bool? PRIORITYFLAG { get; set; }
            public bool SENDFLAG { get; set; }
        }

        public class LocationAssignment
        {
            public string JOBID { get; set; }
            public string ITEMCODE { get; set; }
            public string LOTNUMBER { get; set; }
            public string LOCATIONCODE { get; set; }
            public string ZONECODE { get; set; }
            public string OLDNOOFBIN { get; set; }
            public string NOOFBIN { get; set; }
            public string OLDLOCATION { get; set; }
            public string OLDSTANDARDQUANTITY { get; set; }
            public string STANDARDQUANTITY { get; set; }
            public string ARROWBINLOCATION { get; set; }
            public string STATUS { get; set; }
            public string REQUESTID { get; set; }
            public string UPDATEDBY { get; set; }

        }

        public class ItemTransferHeader
        {
            public string ITEMTRANSFERID { get; set; }
            public string WAREHOUSECODE { get; set; }
            public string TENANTCODE { get; set; }
            public string ITEMCODE { get; set; }
            public string LOTNUMBER { get; set; }
            public string TOLOCATIONCODE { get; set; }
            public string FROMLOCATIONCODE { get; set; }
            public string BINOKQTY { get; set; }
            public int ARROWBINLOCATION { get; set; }
            public string UPDATEDBY { get; set; }
        }

        public class SalesRouteDelivery
        {
            public string TENANTCODE { get; set; }
            public string WAREHOUSECODE { get; set; }
            public string DELIVERYNO { get; set; }
            public string SHIPPERNAME { get; set; }
            public string SHIPPERADDRESS { get; set; }
            public string ROUTE { get; set; }
            public string VEHICLENO { get; set; }
            public string JOBNO { get; set; }
            public string REFERENCE { get; set; }
        }

        public class P1BP2PHP
        {
            public string PHCRDT { get; set; }
            public string PHJBID { get; set; }
            public string PHINVN { get; set; }
            public string PHCASE { get; set; }
            public string PHNRC2 { get; set; }
            public string PHNRC3 { get; set; }
            public string PHUSER { get; set; }
            public string PHUPDT { get; set; }
            public string PHUPTM { get; set; }
            public string PHSTAT { get; set; }
            public string PHFPNT { get; set; }

        }

        public class P1LOC2P
        {
            public string L2JBID { get; set; }
            public string L2FRAN { get; set; }
            public string L2PNO { get; set; }
            public string L2OLOC { get; set; }
            public string L2NLOC { get; set; }
            public string L2OBOX { get; set; }
            public string L2NBOX { get; set; }
            public string L2OQBX { get; set; }
            public string L2NQBX { get; set; }
            public string L2FLAG { get; set; }
            public string L2FPNT { get; set; }
            public string L2CRDT { get; set; }
            public string L2CRTM { get; set; }
            public string L2USER { get; set; }
            public string L2UPDT { get; set; }
            public string L2UPTM { get; set; }
        }


        //Shipping Submission
        public class P1BCCHP
        {
            public string CHJBID { get; set; }
            public string CHMODE { get; set; }
            public string CHCUNO { get; set; }
            public string CHNRC2 { get; set; }
            public string CHNRC3 { get; set; }
            public string CHFLAG { get; set; }
            public string CHCRDT { get; set; }
            public string CHCRTM { get; set; }
            public string CHUSER { get; set; }
            public string CHUPDT { get; set; }
            public string CHUPTM { get; set; }
        }

        public class P1BCC2P
        {
            public string CDJBID { get; set; }
            public string CDCASE { get; set; }
            public string CDBHNO { get; set; }
            public string CDBITM { get; set; }
            public string CDFRAN { get; set; }
            public string CDPNO { get; set; }
            public string CDAQTY { get; set; }
            public string CDFPNT { get; set; }
            public string CDCUNO { get; set; }
        }

        public class P1BCC3P
        {
            public string CDJBID { get; set; }
            public string CDBHNO { get; set; }
            public string CDBITM { get; set; }
            public string CDFRAN { get; set; }
            public string CDPNO { get; set; }
            public string CDDQTY { get; set; }
            public string CDRSCD { get; set; }
            public string CDFPNT { get; set; }
        }
        // End Shipping

        //EM Submission
        public class P1FD01HP
        {
            public string FHFLID { get; set; }
            public string FHJBID { get; set; }
            public string FHSINV { get; set; }
            public string FHNRC2 { get; set; }
            public string FHNRC3 { get; set; }
            public string FHFLAG { get; set; }
            public string FHCRDT { get; set; }
            public string FHCRTM { get; set; }
            public string FHUSER { get; set; }
            public string FHUPDT { get; set; }
            public string FHUPTM { get; set; }

        }

        public class P1FD01TP
        {
            public string FTFLID { get; set; }
            public string FTJBID { get; set; }
            public string FTBHNO { get; set; }
            public string FTBITM { get; set; }
            public string FTCASE { get; set; }
            public string FTFRAN { get; set; }
            public string FTPNO { get; set; }
            public string FTQTY { get; set; }
            public string FTETAD { get; set; }
            public string FTETAP { get; set; }
            public string FTSPDT { get; set; }
            public string FTGWGT { get; set; }
            public string FTNWGT { get; set; }
            public string FTMEAS { get; set; }
            public string FTSINV { get; set; }
            public string FTVSCD { get; set; }
            public string FTCNTN { get; set; }
            public string FTVNME { get; set; }
            public string FTERCD { get; set; }

        }

        public class P1FD013P
        {
            public string FTJBID { get; set; }
            public string FTBHNO { get; set; }
            public string FTBITM { get; set; }
            public string FTFRAN { get; set; }
            public string FTPNO { get; set; }
            public string FTDNQT { get; set; }
            public string FTRSCD { get; set; }
            public string FTERCD { get; set; }

        }
        // End EM Shipping


        // Exports Submission
        public class P1BCEXIP
        {
            public string EHJBID { get; set; }
            public string EHVSNM { get; set; }
            public string EHNRC2 { get; set; }
            public string EHNRC3 { get; set; }
            public string EHFLAG { get; set; }
            public string EHCRDT { get; set; }
            public string EHCRTM { get; set; }
            public string EHUSER { get; set; }
            public string EHUPDT { get; set; }
            public string EHUPTM { get; set; }
        }

        public class P1BCEX2P
        {
            public string EXJBID { get; set; }
            public string EXBHNO { get; set; }
            public string EXBITM { get; set; }
            public string EXFRAN { get; set; }
            public string EXPNO { get; set; }
            public string EXPQTY { get; set; }
            public string EXCASE { get; set; }
            public string EXVSNM { get; set; }
            public string EXCNTN { get; set; }
            public string EXNETW { get; set; }
            public string EXGRWT { get; set; }
            public string EXMEAS { get; set; }
            public string EXMCAS { get; set; }
            public string EXETAD { get; set; }
            public string EXSHPD { get; set; }
            public string EXSTS { get; set; }
        }

        public class P1BCEX3P
        {
            public string EXJBID { get; set; }
            public string EXBHNO { get; set; }
            public string EXBITM { get; set; }
            public string EXFRAN { get; set; }
            public string EXPNO { get; set; }
            public string EXDQTY { get; set; }
            public string EXRSCD { get; set; }
            public string EXDSTS { get; set; }

        }
        //End Exports Submission
        public class P1CFIVP
        {
            public string CIJBID { get; set; }
            public string EXBHNO { get; set; }
            public string CISORC { get; set; }
            public string CIFLAG { get; set; }
            public string CIINVN { get; set; }
            public string CIUPDT { get; set; }
            public string CIUPTM { get; set; }

        }

        public class RCV
        {
            public string ORDERNUMBER { get; set; }
            public string ITEMCODE { get; set; }
            public string ITEMUOMCODE { get; set; }
            public string BATCHNUMBER { get; set; }
            public int CHECKLEQTY { get; set; }
            public int BINLEQTY { get; set; }
        }

        public class Parameter
        {
            public string DESCRIPTION { get; set; }
            public string VALUE { get; set; }
        }


        public static string TopasDate(DateTime datestamp)
        {
            if (datestamp == null) datestamp = System.DateTime.Now;
            return datestamp.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
        }

        public static string TopasTime(DateTime datestamp)
        {
            if (datestamp == null) datestamp = System.DateTime.Now;
            return DateTime.Now.ToString("HHmmss", System.Globalization.CultureInfo.InvariantCulture);
        }

        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    //in case you have a enum/GUID datatype in your model
                    //We will check field's dataType, and convert the value in it.
                    if (pro.Name == column.ColumnName)
                    {
                        try
                        {
                            var convertedValue = GetValueByDataType(pro.PropertyType, dr[column.ColumnName]);
                            pro.SetValue(obj, convertedValue, null);
                        }
                        catch (Exception e)
                        {
                            //ex handle code                   
                            throw;
                        }
                        //pro.SetValue(obj, dr[column.ColumnName], null);
                    }
                    else
                        continue;
                }
            }
            return obj;
        }

        private static object GetValueByDataType(Type propertyType, object o)
        {
            if (o.ToString() == "null")
            {
                return null;
            }
            if (propertyType == (typeof(Guid)) || propertyType == typeof(Guid?))
            {
                return Guid.Parse(o.ToString());
            }
            else if (propertyType == typeof(int) || propertyType.IsEnum)
            {
                return Convert.ToInt32(o);
            }
            else if (propertyType == typeof(decimal))
            {
                return Convert.ToDecimal(o);
            }
            else if (propertyType == typeof(long))
            {
                return Convert.ToInt64(o);
            }
            else if (propertyType == typeof(bool) || propertyType == typeof(bool?))
            {
                return Convert.ToBoolean(o);
            }
            else if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
            {
                return Convert.ToDateTime(o);
            }
            return o.ToString();
        }

        public static SqlConnection GudangSysConnection()
        {
            SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["gudangsysconnection"].ToString());
            return Connection;
        }

        public static SqlConnection TopasConnection()
        {
            SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["topasconnection"].ToString());
            return Connection;
        }

        public static iDB2Connection Db2GudangSysConnection()
        {
            iDB2Connection Connection = new iDB2Connection(ConfigurationManager.ConnectionStrings["gudangsysconnection"].ToString());
            return Connection;
        }

        public static iDB2Connection Db2TopasConnection()
        {
            iDB2Connection Connection = new iDB2Connection(ConfigurationManager.ConnectionStrings["topasconnection"].ToString());
            return Connection;
        }

        public static DataTable GetDataTable(SqlCommand sqlCmd, SqlConnection connection)
        {

            SqlConnection dBConn = new SqlConnection();
            SqlDataAdapter sqlDtAdapter = new SqlDataAdapter();
            SqlCommandBuilder sqlCmbBuilder = new SqlCommandBuilder();

            DataTable dtTable = new DataTable();
            dBConn = connection;
            try
            {
                dtTable = new DataTable();
                if (dBConn.State != ConnectionState.Open)
                    dBConn.Open();

                sqlCmd.Connection = dBConn;
                sqlCmbBuilder.DataAdapter = sqlDtAdapter;
                sqlDtAdapter.SelectCommand = sqlCmd;
                sqlDtAdapter.Fill(dtTable);
                sqlDtAdapter.Dispose();
                sqlCmd.Dispose();
                sqlCmbBuilder.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dBConn.Close();
            }

            return dtTable;
        }


        public static bool RunCmd(string cmdText, iDB2Connection connection)
        {
            bool rc = true;

            // Construct a string which contains the call to QCMDEXC.


            using (connection)
            {
                using (iDB2Command cmd = new iDB2Command(cmdText, connection))
                {
                    try { cmd.ExecuteNonQuery(); }
                    catch (Exception e)
                    {
                        Console.WriteLine("CALL COMMMAND ERROR: " + e.Message);
                        rc = false;
                    }
                }
            }

            // Return success or failure
            return rc;
        }

        public static DataTable Db2GetDataTable(iDB2Command sqlCmd, iDB2Connection connection)
        {
            iDB2Connection dBConn = new iDB2Connection();
            iDB2DataAdapter sqlDtAdapter = new iDB2DataAdapter();
            iDB2CommandBuilder sqlCmbBuilder = new iDB2CommandBuilder();

            if (ConfigurationManager.AppSettings["Debug"].ToString() == "On") Console.WriteLine(GetActualQuery(sqlCmd));
            DataTable dtTable = new DataTable();
            dBConn = connection;
    
            try
            {
                dtTable = new DataTable();
                if (dBConn.State != ConnectionState.Open)
                    dBConn.Open();

                sqlCmd.Connection = dBConn;
                sqlCmbBuilder.DataAdapter = sqlDtAdapter;
                sqlDtAdapter.SelectCommand = sqlCmd;
                sqlDtAdapter.Fill(dtTable);
                sqlDtAdapter.Dispose();
                sqlCmd.Dispose();
                sqlCmbBuilder.Dispose();
            }
            catch (Exception e)
            {
                var st = new StackTrace(e, true);
                // Get the top stack frame
                var frame = st.GetFrame(st.FrameCount - 1);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("Error [" + DateTime.Now.ToString() + "] : " + e.Message);
                Console.WriteLine("Line Number [" + DateTime.Now.ToString() + "] : " + line.ToString());
                Console.WriteLine("Inner Exception: " + e.InnerException == null ? e.InnerException.ToString() : "");
                Console.ForegroundColor = ConsoleColor.White;
            }
            finally
            {
                dBConn.Close();
            }

            return dtTable;

        }

        private static string GetActualQuery(iDB2Command sqlcmd)
        {
            string query = sqlcmd.CommandText;
            string parameters = "";
            string[] strArray = System.Text.RegularExpressions.Regex.Split(query, " VALUES ");

            //Reconstructs the second half of the SQL Command
            parameters = "(";

            int count = 0;
            foreach (iDB2Parameter p in sqlcmd.Parameters)
            {
                if (count == (sqlcmd.Parameters.Count - 1))
                {
                    parameters += "'" + p.Value.ToString() + "'";
                }
                else
                {
                    parameters += "'" + p.Value.ToString() + "', ";
                }
                count++;
            }

            parameters += ")";

            //Returns the string recombined.
            return strArray[0] + " VALUES " + parameters;
        }

        private static bool ftp(string cmd)
        {

            try
            {
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Ftp,
                    HostName = ConfigurationManager.AppSettings["Hostname"].ToString(),
                    UserName = ConfigurationManager.AppSettings["Userid"].ToString(),
                    Password = ConfigurationManager.AppSettings["Password"].ToString()
                };

                using (Session session = new Session())
                {
                    // Connect
                    session.Open(sessionOptions);

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    CommandExecutionResult transferResult;
                    transferResult =
                        session.ExecuteCommand(cmd);
                    //session.PutFiles(@localpath, "/", false, transferOptions);

                    // Throw on any error
                    transferResult.Check();
                    Console.WriteLine(cmd + " - Complete");

                }
                return (true);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e);
                return (false);
            }
        }


        private static bool UpdateTopasStatus(string packagename)
        {
            var TopasSchema = ConfigurationManager.AppSettings["TopasSchema"].ToString();
            iDB2Command updateCmd = new iDB2Command();

            updateCmd.CommandText = "INSERT INTO " + TopasSchema + ".P1TBIPSP   " +
                "(TBTASK ,TBDATE ,TBTIME) " +
                "VALUES ('" + packagename + "',@BHUPDT,@BHUPTM )";

            updateCmd.Parameters.AddWithValue("@BHUPDT", TopasDate(DateTime.Now));
            updateCmd.Parameters.AddWithValue("@BHUPTM", TopasTime(DateTime.Now));
            Db2GetDataTable(updateCmd, Db2TopasConnection());
            return true;
        }


        static void ExecuteCommand(string command)
        {

            Console.WriteLine("Executing : " + command);
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            process.WaitForExit();

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            exitCode = process.ExitCode;

            Console.WriteLine("output>>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            Console.WriteLine("error>>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
            Console.WriteLine("ExitCode: " + exitCode.ToString(), "ExecuteCommand");
            process.Close();
        }

    }




}
